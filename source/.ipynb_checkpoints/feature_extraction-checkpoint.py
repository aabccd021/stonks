import functools
import re
import numpy as np
from pipeline import main_pipe

def sequence_extract(feats:list=[], params=[]):
    print('params', params)
    return functools.partial(_sequence_extract, feats=feats, par=params)


@main_pipe('Seq')
def _sequence_extract(X_train, X_test, y_train, y_test, cat_feats:list, *args, **kwargs):
    feats = kwargs['feats']
    params = kwargs['par']
    for feat in feats:
        def str_to_seq(inn):
            return [float(x) for x in inn.replace("'","").replace('[','').replace(']','').split(', ')]

        train_seq = X_train[feat].apply(str_to_seq)
        test_seq = X_test[feat].apply(str_to_seq)

        if 'mean' in params:
            X_train['{}_mean'.format(feat)] = train_seq.apply(np.mean)
            X_test['{}_mean'.format(feat)] = test_seq.apply(np.mean)

        if 'median' in params:
            X_train['{}_median'.format(feat)] = train_seq.apply(np.median)
            X_test['{}_median'.format(feat)] = test_seq.apply(np.median)

        if 'last' in params:
            X_train['{}_last'.format(feat)] = train_seq.apply(lambda x: x[-1])
            X_test['{}_last'.format(feat)] = test_seq.apply(lambda x: x[-1])

        if 'first' in params:
            X_train['{}_first'.format(feat)] = train_seq.apply(lambda x: x[0])
            X_test['{}_first'.format(feat)] = test_seq.apply(lambda x: x[0])
        
        if 'len' in params:
            X_train['{}_len'.format(feat)] = train_seq.apply(lambda x: len(x))
            X_test['{}_len'.format(feat)] = test_seq.apply(lambda x: len(x))

        if 'max' in params:
            X_train['{}_max'.format(feat)] = train_seq.apply(lambda x: max(x))
            X_test['{}_max'.format(feat)] = test_seq.apply(lambda x: max(x))

        if 'min' in params:
            X_train['{}_min'.format(feat)] = train_seq.apply(lambda x: min(x))
            X_test['{}_min'.format(feat)] = test_seq.apply(lambda x: min(x))
        
        r = re.compile("^quant")
        newlist = list(filter(r.match, params)) # Read Note
        if len(newlist) > 0:
            for quant_num in newlist:
                hist_num = int(quant_num.replace('quant',''))
                X_train['{}_q{}'.format(feat, quant_num)] = train_seq.apply(lambda x: min(x))
                X_test['{}_q{}'.format(feat, quant_num)] = test_seq.apply(lambda x: min(x))
        
        # X_train = X_train.drop(feat, 1)
        # X_test = X_test.drop(feat, 1)
        cat_feats = cat_feats - set([feat])
        # print(cat_feats)
        # assert 1==2

        return X_train, X_test, y_train, y_test, cat_feats


def count_occurence(feats:list=[]):
    return functools.partial(_count_occurence, feats=feats)

@main_pipe('cnt')
def _count_occurence(X_train, X_test, y_train, y_test, cat_feats:list, *args, **kwargs):
    feats = kwargs['feats']
    for feat in feats:
        train_unique, train_counts = np.unique(X_train[feat], return_counts=True)
        test_unique, test_counts = np.unique(X_test[feat], return_counts=True)

        train_mapping = dict(zip(train_unique, train_counts))
        test_mapping = dict(zip(test_unique, test_counts))

        X_train["{}_count".format(feat)] = X_train[feat].apply(lambda x: train_mapping[x])
        X_test["{}_count".format(feat)] = X_test[feat].apply(lambda x: test_mapping[x])

    return X_train, X_test, y_train, y_test, cat_feats