from stonks import Stonks
from pipeline import fit_predict, preproc_pipe, main_pipe
from feature_extraction import sequence_extract, count_occurence
from feature_selections import remove_zero_variance_column, manual_drop
from preprocessing import remove_not_in_test, set_index
from sklearn.metrics import f1_score
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC, LinearSVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.linear_model import LogisticRegression
from catboost import CatBoostClassifier
import time
import numpy as np
import pandas as pd
from tqdm import tqdm
import concurrent.futures
from joblib import Parallel, delayed  
from normalization import box_cox
from sampling import smotenc


@preproc_pipe('fix')
def column_name_fix(train, test, submit, cat_feats, *args, **kwargs):
    new_train = train.rename(columns={'hotel_id': 'is_cross_sell'})
    new_train['is_cross_sell'] = new_train['is_cross_sell'].apply(
        lambda x: 'no' if x == 'None' else 'yes')
    print('renamed train dataframe column hotel_id => is_cross_sell')
    return new_train, test, submit, cat_feats

def parallel_list_map(f, my_iter):
    with concurrent.futures.ThreadPoolExecutor() as executor:
        results = list(tqdm(executor.map(f, my_iter), total=len(my_iter)))
    return results

@main_pipe('AcOcc')
def count_account_id_occured(X_train, X_test, y_train, y_test, cat_feats:list, *args, **kwargs):
    un_train = X_train['account_id'].unique()
    un_test = X_test['account_id'].unique()

    acc_dfs = []
    for acc in tqdm(un_train):
        acc_df = X_train[X_train['account_id']==acc][['log_transaction']]
        acc_df['log_len'] = acc_df['log_transaction'].apply(lambda a: len([x for x in a.replace("'","").replace('[','').replace(']','').split(', ')]))
        acc_df = acc_df.sort_values(by=['log_len'])
        acc_df['acc_occured'] = range(1,acc_df.shape[0]+1)
        acc_df = acc_df[['acc_occured']]
        acc_dfs.append(acc_df)
    acc_dfs = pd.concat(acc_dfs)
    X_train = X_train.join(acc_dfs)

    acc_dfs = []
    for acc in tqdm(un_test):
        acc_df = X_test[X_test['account_id']==acc][['log_transaction']]
        acc_df['log_len'] = acc_df['log_transaction'].apply(lambda a: len([x for x in a.replace("'","").replace('[','').replace(']','').split(', ')]))
        acc_df = acc_df.sort_values(by=['log_len'])
        acc_df['acc_occured'] = range(1,acc_df.shape[0]+1)
        acc_df = acc_df[['acc_occured']]
        acc_dfs.append(acc_df)
    acc_dfs = pd.concat(acc_dfs)
    X_test = X_test.join(acc_dfs)

    return X_train, X_test, y_train, y_test, cat_feats

@main_pipe('extPl')
def extract_places(X_train, X_test, y_train, y_test, cat_feats:list, *args, **kwargs):
    train_vc = X_train['visited_city'].values

    semarang = []
    jakarta = []
    medan = []
    bali = []
    jogjakarta = []
    manado = []
    aceh = []

    for city in train_vc:
        if 'Semarang' in city:
            semarang.append(1)
        else:
            semarang.append(0)

        if 'Jakarta' in city:
            jakarta.append(1)
        else:
            jakarta.append(0)

        if 'Medan' in city:
            medan.append(1)
        else:
            medan.append(0)

        if 'Bali' in city:
            bali.append(1)
        else:
            bali.append(0)

        if 'Jogjakarta' in city:
            jogjakarta.append(1)
        else:
            jogjakarta.append(0)

        if 'Manado' in city:
            manado.append(1)
        else:
            manado.append(0)

        if 'Aceh' in city:
            aceh.append(1)
        else:
            aceh.append(0)
    X_train['Semarang'] = semarang
    X_train['Jakarta'] = jakarta
    X_train['Medan'] = medan
    X_train['Bali'] =bali
    X_train['Jogjakarta'] =jogjakarta
    X_train['Manado'] =manado
    X_train['Aceh'] = aceh

    test_vc = X_test['visited_city'].values

    semarang = []
    jakarta = []
    medan = []
    bali = []
    jogjakarta = []
    manado = []
    aceh = []

    for city in test_vc:
        if 'Semarang' in city:
            semarang.append(1)
        else:
            semarang.append(0)

        if 'Jakarta' in city:
            jakarta.append(1)
        else:
            jakarta.append(0)

        if 'Medan' in city:
            medan.append(1)
        else:
            medan.append(0)

        if 'Bali' in city:
            bali.append(1)
        else:
            bali.append(0)

        if 'Jogjakarta' in city:
            jogjakarta.append(1)
        else:
            jogjakarta.append(0)

        if 'Manado' in city:
            manado.append(1)
        else:
            manado.append(0)

        if 'Aceh' in city:
            aceh.append(1)
        else:
            aceh.append(0)

    X_test['Semarang'] = semarang
    X_test['Jakarta'] = jakarta
    X_test['Medan'] = medan
    X_test['Bali'] =bali
    X_test['Jogjakarta'] =jogjakarta
    X_test['Manado'] =manado
    X_test['Aceh'] = aceh


    return X_train, X_test, y_train, y_test, cat_feats

if __name__ == "__main__":
    start = time.time()

    models = [
        # (CatBoostClassifier, {'iterations':100, 'loss_function':'MultiClass'}),
        # (CatBoostClassifier, {'iterations':100, 'loss_function':'MultiClassOneVsAll'}),
        # (CatBoostClassifier, {'iterations':100, 'loss_function':'CrossEntropy'}),
        # (CatBoostClassifier, {'iterations':100,'scale_pos_weight':20}),
        # (CatBoostClassifier, {'iterations':100}),
        # (CatBoostClassifier, {'iterations':1000,'scale_pos_weight':8}),
        # (KNeighborsClassifier, {}),
        # (DecisionTreeClassifier, {}),
        # (RandomForestClassifier, {}),
        (RandomForestClassifier, {'n_estimators':100, 'max_features':'log2','n_jobs':-1}),
        # (RandomForestClassifier, {'n_estimators':100, 'max_features':0.8}),
        # (RandomForestClassifier, {'n_estimators':100, 'max_features':0.8}),
        # (RandomForestClassifier, {'n_estimators':100, 'class_weight':'balanced'}),
        # (RandomForestClassifier, {'n_estimators':200, 'max_features':0.8,'n_jobs':-1}),
        # (RandomForestClassifier, {'n_estimators':100, 'max_features':0.5,'n_jobs':-1}),
        # (RandomForestClassifier, {'n_estimators':100, 'max_features':0.2,'n_jobs':-1}),
        # (RandomForestClassifier, {'n_estimators':100, 'bootstrap':True,'n_jobs':-1}),
        # (RandomForestClassifier, {'n_estimators':100, 'class_weight':'balanced'}),
        # (RandomForestClassifier, {'n_estimators':150}),
        # (RandomForestClassifier, {'n_estimators':150, 'class_weight':'balanced'}),
        # (RandomForestClassifier, {'n_estimators':200}),
        # (RandomForestClassifier, {'n_estimators':200, 'class_weight':'balanced'}),
        # (RandomForestClassifier, {'n_estimators':100, 'class_weight':'balanced_subsample'}),
        # (MLPClassifier, {'alpha':1, 'max_iter':100}),
        # (AdaBoostClassifier, {}),
        # (GaussianNB, {}),scale
        # (QuadraticDiscriminantAnalysis, {}),
        # (LogisticRegression, {'multi_class':'ovr'}),
        # (LogisticRegression, {}),
        # (LinearSVC, {})
        # (SVC,{'C':1,'gamma':1,'class_weight':None}),
        # (SVC,{'C':1,'gamma':1,'class_weight':'auto'}),
    ]

    seq_var = [
        # sequence_extract(feats=['log_transaction'], params=['mean']),
        # sequence_extract(feats=['log_transaction'], params=['median']),
        # sequence_extract(feats=['log_transaction'], params=['last']),
        # sequence_extract(feats=['log_transaction'], params=['first','len']),
        # sequence_extract(feats=['log_transaction'], params=['first','last']),
        # sequence_extract(feats=['log_transaction'], params=['first']),
        sequence_extract(feats=['log_transaction'], params=['first','max']),
        # sequence_extract(feats=['log_transaction'], params=['first','max','len']),
        # sequence_extract(feats=['log_transaction'], params=['first','max','last']),
        # sequence_extract(feats=['log_transaction'], params=['first','max','first']),
        # sequence_extract(feats=['log_transaction'], params=['first','max','min']),
        # sequence_extract(feats=['log_transaction'], params=['first','max','median']),
        # sequence_extract(feats=['log_transaction'], params=['first','max','q0.4']),
        # sequence_extract(feats=['log_transaction'], params=['first','max','q0.6']),
        # sequence_extract(feats=['log_transaction'], params=['first','max','q0.8']),
        # sequence_extract(feats=['log_transaction'], params=['q0.2']),
        # sequence_extract(feats=['log_transaction'], params=['q0.4']),
        # sequence_extract(feats=['log_transaction'], params=['q0.6']),
        # sequence_extract(feats=['log_transaction'], params=['q0.8']),
        # sequence_extract(feats=['log_transaction'], params=['max']),
        # sequence_extract(feats=['log_transaction'], params=['min']),
        # sequence_extract(feats=['log_transaction'], params=['len']),
        # sequence_extract(feats=['log_transaction'], params=[]),
    ]

    for seq in seq_var:

        for model, params in models:
            print(model)
            preprocess = [
                set_index,
                column_name_fix,
                remove_not_in_test,
            ]

            main = [
                # count_account_id_occured,
                seq,
                # count_occurence(feats=['account_id']),
                manual_drop(feats=['account_id','log_transaction']),
                remove_zero_variance_column,
                # box_cox(['member_duration_days']),
                # smotenc,
                extract_places,
                # manual_drop(feats=['visited_city']),
                fit_predict(
                    model=model,
                    model_kwargs=params,
                )
            ]

            stonks = Stonks(
                target_var_name='is_cross_sell',
                index_var_name='order_id',
                train_file_name='flight.csv',
                cv=5,
                preprocess=preprocess,
                eval_func=f1_score,
                eval_kwargs={
                    'pos_label': 'yes',
                    'average': 'macro',
                },
            )

            stonks.eval(main)

    print("total time:", time.time()-start)
