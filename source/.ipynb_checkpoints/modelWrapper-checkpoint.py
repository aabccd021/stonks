import sklearn
from utils import get_pipe_string, load_val_data, save_fold_result, \
    get_ori_submit_csv, save_submit, fold_result_exists, submit_exists, \
    load_val_cat_feats, get_int_mapping, map_cat_to_int, map_int_to_cat, \
        get_cat_feats_indices
import pandas as pd
from pipeline import main_pipe
import numpy as np
import catboost


class StonksWrapper:
    @staticmethod
    def get_model(ori_model, model_kwargs):
        if issubclass(ori_model, sklearn.base.BaseEstimator):
            return StonksSklearnWrapper(ori_model, model_kwargs)
        elif issubclass(ori_model, catboost.core.CatBoost):
            return StonksCatboostWrapper(ori_model, model_kwargs)
        else:
            import inspect
            print(inspect.getmro(ori_model))
            assert 1 == 2
            return StonksWrapper

    def __init__(self, ori_model, model_kwargs):
        self.model_kwargs = model_kwargs
        self.ori_model = ori_model(**model_kwargs)

    def fit_predict_eval(self, input_pipe, stonks_info, fit_kwargs={}):
        eval_func = stonks_info['eval_func']
        eval_kwargs = stonks_info['eval_kwargs']

        model_params_string = ""
        for arg in self.model_kwargs:
            if arg == 'n_jobs':
                continue
            model_params_string += compress_args("{}:{},".format(arg,
                                                   self.model_kwargs[arg]).replace("'", ""))
        model_capital_name = ''.join(
            [c for c in self.ori_model.__class__.__name__ if c.isupper()])
        result_pipe = input_pipe + \
            ["{}{}".format(model_capital_name,
                           model_params_string)]
        result_name = get_pipe_string(result_pipe)

        if not fold_result_exists(result_name):
            data_name = get_pipe_string(input_pipe)
            X_train, X_test, y_train, y_test, cat_feats = load_val_data(
                data_name)

            print('feats', X_train.columns)

            mapping = get_int_mapping(y_train)

            self.ori_model.fit(X_train, map_cat_to_int(
                y_train, mapping).values.ravel(), **fit_kwargs)
            y_pred = self.ori_model.predict(X_test).flatten()

            y_pred = map_int_to_cat(y_pred, mapping)

            assert len(y_pred) == y_test.shape[0]
            eval_result = eval_func(y_test, y_pred, **eval_kwargs)
            # assert eval_result > 0

            save_fold_result(result_name, X_train, X_test,
                             y_train, y_test, y_pred, cat_feats, eval_result)
            print(eval_result)
        return result_pipe

    def fit_predict_submit(self, input_pipe, stonks_info, fit_kwargs={}):
        model_params_string = ""
        for arg in self.model_kwargs:
            if arg == 'n_jobs':
                continue
            model_params_string += compress_args("{}:{},".format(arg,
                                                   self.model_kwargs[arg]).replace("'", ""))

        model_capital_name = ''.join(
            [c for c in self.ori_model.__class__.__name__ if c.isupper()])
        output_pipe = input_pipe + \
            ["{}{}".format(model_capital_name,
                           model_params_string)]
        submit_name = get_pipe_string(output_pipe)

        if not submit_exists(submit_name):

            data_name = get_pipe_string(input_pipe)
            X_train, X_test, y_train, y_test, cat_feats = load_val_data(
                data_name)

            print('feats', X_train.columns)

            mapping = get_int_mapping(y_train)
            self.ori_model.fit(X_train, map_cat_to_int(
                y_train, mapping).values.ravel(), **fit_kwargs)
            y_pred = self.ori_model.predict(X_test).flatten()
            y_pred = map_int_to_cat(y_pred, mapping)

            # self.ori_model.fit(X_train, y_train.values.ravel(), **fit_kwargs)
            # y_pred = self.ori_model.predict(X_test)

            sample_submission = get_ori_submit_csv()

            assert X_test.shape[0] == sample_submission.shape[0]
            index_var = stonks_info['index_var_name']
            assert (X_test.index.values ==
                    sample_submission[index_var].values).all()
            assert np.array_equal(X_test.index.values,
                                  sample_submission[index_var].values)

            target = stonks_info['target_var_name']
            sample_submission[target] = y_pred
            submission = sample_submission

            assert type(submission) == pd.DataFrame
            save_submit(submit_name, X_train, X_test,
                        y_train, submission, cat_feats)

        return output_pipe


class StonksSklearnWrapper(StonksWrapper):

    def fit_predict_eval(self, input_pipe, stonks_info, fit_kwargs={}):
        new_pipe = one_hot(pipes=input_pipe)
        return super().fit_predict_eval(new_pipe, stonks_info, fit_kwargs=fit_kwargs)

    def fit_predict_submit(self, input_pipe, stonks_info, fit_kwargs={}):
        new_pipe = one_hot(pipes=input_pipe)
        return super().fit_predict_submit(new_pipe, stonks_info, fit_kwargs=fit_kwargs)


class StonksCatboostWrapper(StonksWrapper):
    def fit_predict_eval(self, input_pipe, stonks_info, fit_kwargs={}):
        fit_kwargs = {
            'cat_features': get_cat_feats_indices(input_pipe),
            'verbose': False,
        }
        return super().fit_predict_eval(input_pipe, stonks_info, fit_kwargs=fit_kwargs)

    def fit_predict_submit(self, input_pipe, stonks_info, fit_kwargs={}):
        fit_kwargs = {
            'cat_features': get_cat_feats_indices(input_pipe),
            'verbose': False,
        }
        return super().fit_predict_submit(input_pipe, stonks_info, fit_kwargs=fit_kwargs)


@main_pipe('1Hot')
def one_hot(X_train, X_test, y_train, y_test, cat_feats, *args, **kwargs):
    for feat in cat_feats:
        X_train = pd.concat([X_train, pd.get_dummies(X_train[feat])], axis=1)
        X_train = X_train.drop(feat, axis=1)
        X_test = pd.concat([X_test, pd.get_dummies(X_test[feat])], axis=1)
        X_test = X_test.drop(feat, axis=1)
    assert X_train.shape[1] == X_test.shape[1]
    return X_train, X_test, y_train, y_test, cat_feats


def compress_args(arg):
    mapping = {
        'estimator': 'est',
        'feature': 'feat',
        'class':'cls',
        'weight':'wgh',
        'multi':'mlt',
        'function':'func',
        'scale':'scl',
    }
    for word, initial in mapping.items():
        arg = arg.replace(word.lower(), initial)
    return arg
