from pipeline import main_pipe
from scipy import stats
import functools

def box_cox(feats=[]):
    return functools.partial(_box_cox, feats=feats)

@main_pipe('bcx')
def _box_cox(X_train, X_test, y_train, y_test, cat_feats:list, *args, **kwargs):
    feats = kwargs['feats']
    for feat in feats:
        a = stats.boxcox(X_train[feat].values)[0]
        b = stats.boxcox(X_test[feat].values)[0]
        X_train[feat] = a
        X_test[feat] = b
    return X_train, X_test, y_train, y_test, cat_feats