from pipeline import preproc_pipe
import numpy as np
import functools
from utils import *


@preproc_pipe('NoTes')
def remove_not_in_test(train, test, submit, cat_feats, *args, **kwargs):

    stonks_info = kwargs['stonks_info']
    index_name = stonks_info['index_var_name']
    target_name = stonks_info['target_var_name']

    rows_before = train.shape[0]

    train_nunique = train.nunique()

    for feat in cat_feats:
        uni_test = set(test[feat].unique())
        train = train[train[feat].isin(uni_test)]
    
    rows_after = train.shape[0]

    print('dropped [{}] rows'.format(rows_before - rows_after))
    
    return train, test, submit, cat_feats

@preproc_pipe('Id')
def set_index(train:pd.DataFrame, test:pd.DataFrame, submit:pd.DataFrame, cat_feats:list, *args, **kwargs):

    stonks_info = kwargs['stonks_info']
    index_name = stonks_info['index_var_name']
    train = train.set_index(index_name)
    test = test.set_index(index_name)
    submit = submit.set_index(index_name)

    return train, test, submit, cat_feats
 

