from utils import is_data_exists, get_data_dir, save_data, load_data, get_feats, get_cv_pipelines, load_val_data, load_fold_result, get_raw_dir, get_submit_pipeline, save_cv_result
from sklearn.model_selection import StratifiedKFold
from pipeline import get_pipe_string
import os
import pandas as pd
import numpy as np


class Stonks():

    def __init__(self,
                 target_var_name, index_var_name,
                 eval_func,
                 eval_kwargs=dict(),
                 cv=3,
                 train_file_name='train.csv', test_file_name='test.csv', submit_file_name='sample_submission.csv',
                 cat_feats: list = [],
                 preprocess: list = [],
                 ):
        self.eval_func = eval_func
        self.eval_kwargs = eval_kwargs
        self.target_var_name = target_var_name
        self.index_var_name = index_var_name
        self.cv = cv
        self.save_raw_as_pickle(
            train_file_name, test_file_name, submit_file_name, cat_feats)
        self.stonks_info = {
            'index_var_name': self.index_var_name,
            'target_var_name': self.target_var_name,
            'eval_func': self.eval_func,
            'eval_kwargs': self.eval_kwargs,
        }
        self.run_preprocess(preprocess)

    def save_raw_as_pickle(self, train_file_name, test_file_name, submit_file_name, cat_feats):
        if is_data_exists('init'):
            return
        data_dir = get_raw_dir()

        train = pd.read_csv(os.path.join(data_dir, 'raw', train_file_name))
        test = pd.read_csv(os.path.join(data_dir, 'raw', test_file_name))
        submit = pd.read_csv(os.path.join(
            data_dir, 'raw', submit_file_name))

        if cat_feats == []:
            cat_feats = self.detect_cat_feats(train)

        save_data('init', train, test, submit, cat_feats)

    def detect_cat_feats(self, train):
        train_nunique = train.nunique()
        cat_cols = set(feat_name for feat_name, feat_type in train.dtypes.to_dict(
        ).items() if feat_type == np.object and train_nunique[feat_name] < 30)

        cat_feats = cat_cols - set([self.index_var_name, self.target_var_name])
        return cat_feats

    def run_preprocess(self, pipeline):
        pipes = []
        for pipe in pipeline:
            pipes = pipe(pipes=pipes, stonks_info=self.stonks_info)
        self.data = pipes

    def eval(self, pipeline):
        cv_pipelines = get_cv_pipelines(
            pipes=self.data, cv=self.cv, stonks_info=self.stonks_info)

        cv_results_pipes = []
        for fold, prev_pipes in enumerate(cv_pipelines):
            print("FOLD{}".format(fold+1))
            pipes = prev_pipes
            for pipe in pipeline:
                pipes = pipe(
                    pipes=pipes, stonks_info=self.stonks_info, mode='eval')
            cv_results_pipes.append(pipes)

        submit_pipeline = get_submit_pipeline(
            pipes=self.data, stonks_info=self.stonks_info)
        pipes = submit_pipeline
        for pipe in pipeline:
            print(pipe)
            pipes = pipe(
                pipes=pipes, stonks_info=self.stonks_info, mode='submit')
        submit_pipe = pipes

        print("RESULTTT")
        scores = []
        for fold, cv_result_pipes in enumerate(cv_results_pipes):
            name = get_pipe_string(cv_result_pipes)
            score = load_fold_result(name)
            scores.append(score)
            print("FOLD{}:  {}".format(fold+1, score))
        scores = np.array(scores)
        scores_mean = scores.mean()
        print("MEAN :  {}".format(scores_mean))

        submit_name = get_pipe_string(submit_pipe)
        save_cv_result(submit_name, scores_mean, self.cv)
