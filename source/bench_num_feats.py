import numpy as np
import time
from catboost import CatBoostRegressor
from preprocessing import remove_not_in_test, set_index, remove_duplicate_column
from feature_selections import remove_zero_variance_column, manual_drop
from feature_extraction import square, log, sqrt, cluster_feats
from pipeline import fit_predict, preproc_pipe, main_pipe
from stonks import Stonks
from impute import fillna


def rmspe(y_true, y_pred):
    squea = np.square((y_true - y_pred)/y_true)
    return np.sqrt(np.mean(squea))


if __name__ == "__main__":
    start = time.time()
    cat_feats = {
        'facility_1',
            'facility_2',
            'facility_3',
            'facility_4',
            'facility_5',
            'female',
            'male',
            'item_1',
            'item_2',
            'item_3',
            'item_4',
            'item_5',
            'secret_1',
            'secret_2',
            'secret_3',
            'secret_4',
            'secret_5',
            'secret_6',
            'secret_7',
            'secret_8',
            'secret_9',
    }
    num_feats =[
        'distance_poi_A1',
        'distance_poi_A2',
        'distance_poi_A3',
        'distance_poi_A4',
        'distance_poi_A5',
        'distance_poi_A6',
        'distance_poi_B1',
        'distance_poi_B2',
        'distance_poi_B3',
        'distance_poi_B4',
        'latitude',
        'longitude',
        'room_size',
    ]

    rist = []

    for feat in num_feats:
        for func in [square, log, sqrt, 
        # manual_drop,
        ]:

            model = CatBoostRegressor
            model_params = {'thread_count': -1, 
            # 'iterations': 460,
                            'verbose': 0, 'loss_function': 'MAE'}
            # model_params = {'thread_count': -1, 'verbose':0}
            print('start', model, model_params)
            preprocess = [
                set_index,
                # remove_not_in_test,
                remove_duplicate_column,
            ]

            main = [
                manual_drop(feats=['secret_10']),
                remove_zero_variance_column,
                fillna(method='catboost'),
                func(feats=[feat],
                drop_original=False
                ),
                fit_predict(
                    model=model,
                    model_kwargs=model_params,
                    # use_one_hot=False,
                )
            ]

            stonks = Stonks(
                target_var_name='price',
                index_var_name='id',
                cv=5,
                preprocess=preprocess,
                eval_func=rmspe,
                problem='regression',
                cat_feats=cat_feats)

            score = stonks.eval(main)
            rist.append((func, feat ,score))

    for el in rist:
        print(el)


    print("total time:", time.time()-start)
