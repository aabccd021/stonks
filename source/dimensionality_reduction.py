from pipeline import main_pipe
from utils import get_feats
import functools
import pandas as pd
from sklearn.decomposition import PCA

def pca(n_components):
    return functools.partial(_pca, n_components=n_components)

@main_pipe('pca')
def _pca(X_train, X_test, y_train, y_test, cat_feats, *args, **kwargs):
    n_components = kwargs['n_components']
    pca = PCA(n_components=n_components)

    num_feats = list(set(X_train.columns) - cat_feats)

    X_train_cat = X_train[list(cat_feats)]
    X_test_cat = X_test[list(cat_feats)]

    X_train_num = X_train[num_feats]
    X_test_num = X_test[num_feats]

    X_train_num = pca.fit_transform(X_train_num).T
    X_test_num = pca.transform(X_test_num).T

    for i in range(X_train_num.shape[0]):
        feat_name = 'PC_{:01d}'.format(i)
        X_train_cat.loc[:,feat_name] = X_train_num[i]
        X_test_cat.loc[:,feat_name] = X_test_num[i]
    
    print(len(X_train.columns))
    print(len(X_train_cat.columns))

    X_train = X_train_cat
    X_test = X_test_cat

    return X_train, X_test, y_train, y_test, cat_feats

