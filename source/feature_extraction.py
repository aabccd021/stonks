import functools
import re
import numpy as np
import pandas as pd
from pipeline import main_pipe
from sklearn.cluster import KMeans

def sequence_extract(feats:list=[], params=[]):
    print('params', params)
    return functools.partial(_sequence_extract, feats=feats, par=params)


@main_pipe('Seq')
def _sequence_extract(X_train, X_test, y_train, y_test, cat_feats:list, *args, **kwargs):
    feats = kwargs['feats']
    params = kwargs['par']
    for feat in feats:
        print('extracting sequence {}'.format(feat))
        def str_to_seq(inn):
            return [float(x) for x in inn.replace("'","").replace('[','').replace(']','').split(', ')]

        train_seq = X_train[feat].apply(str_to_seq)
        test_seq = X_test[feat].apply(str_to_seq)

        if 'mean' in params:
            X_train['{}_mean'.format(feat)] = train_seq.apply(np.mean)
            X_test['{}_mean'.format(feat)] = test_seq.apply(np.mean)

        if 'median' in params:
            X_train['{}_median'.format(feat)] = train_seq.apply(np.median)
            X_test['{}_median'.format(feat)] = test_seq.apply(np.median)

        if 'last' in params:
            X_train['{}_last'.format(feat)] = train_seq.apply(lambda x: x[-1])
            X_test['{}_last'.format(feat)] = test_seq.apply(lambda x: x[-1])

        if 'first' in params:
            X_train['{}_first'.format(feat)] = train_seq.apply(lambda x: x[0])
            X_test['{}_first'.format(feat)] = test_seq.apply(lambda x: x[0])
        
        if 'len' in params:
            X_train['{}_len'.format(feat)] = train_seq.apply(lambda x: len(x))
            X_test['{}_len'.format(feat)] = test_seq.apply(lambda x: len(x))

        if 'max' in params:
            X_train['{}_max'.format(feat)] = train_seq.apply(lambda x: max(x))
            X_test['{}_max'.format(feat)] = test_seq.apply(lambda x: max(x))

        if 'min' in params:
            X_train['{}_min'.format(feat)] = train_seq.apply(lambda x: min(x))
            X_test['{}_min'.format(feat)] = test_seq.apply(lambda x: min(x))
        
        r = re.compile("^quant")
        newlist = list(filter(r.match, params)) # Read Note
        if len(newlist) > 0:
            for quant_num in newlist:
                hist_num = int(quant_num.replace('quant',''))
                X_train['{}_q{}'.format(feat, quant_num)] = train_seq.apply(lambda x: min(x))
                X_test['{}_q{}'.format(feat, quant_num)] = test_seq.apply(lambda x: min(x))
        
        # X_train = X_train.drop(feat, 1)
        # X_test = X_test.drop(feat, 1)
        cat_feats = cat_feats - set([feat])
        # print(cat_feats)
        # assert 1==2

    return X_train, X_test, y_train, y_test, cat_feats


def count_occurence(feats:list=[]):
    return functools.partial(_count_occurence, feats=feats)

@main_pipe('count occurence')
def _count_occurence(X_train, X_test, y_train, y_test, cat_feats:list, *args, **kwargs):
    feats = kwargs['feats']
    for feat in feats:
        train_unique, train_counts = np.unique(X_train[feat], return_counts=True)
        test_unique, test_counts = np.unique(X_test[feat], return_counts=True)

        train_mapping = dict(zip(train_unique, train_counts))
        test_mapping = dict(zip(test_unique, test_counts))

        X_train["{}_count".format(feat)] = X_train[feat].apply(lambda x: train_mapping[x])
        X_test["{}_count".format(feat)] = X_test[feat].apply(lambda x: test_mapping[x])

    return X_train, X_test, y_train, y_test, cat_feats

def target_encoding(feats:list=[], drop_original=True):
    return functools.partial(_target_encoding, feats=feats, drop_original=drop_original)

@main_pipe('target encoding')
def _target_encoding(X_train, X_test, y_train, y_test, cat_feats:list, *args, **kwargs):
    feats = kwargs['feats']
    target = kwargs['stonks_info']['target_var_name']
    drop_original = kwargs['drop_original']

    cat_feats = set(cat_feats)
    for feat in feats:
        assert feat in cat_feats
        feat_unique_val = np.unique(X_train[feat])
        val_mean_dict = dict()
        for val in feat_unique_val:
            temp_X_train = X_train.copy(deep=True)
            temp_X_train[target] = y_train
            mean = temp_X_train.loc[temp_X_train[feat]==val][target].mean()
            val_mean_dict[val] = mean
        X_train['{}_target_mean'.format(feat)] = X_train[feat].apply(lambda x: val_mean_dict[x])
        X_test['{}_target_mean'.format(feat)] = X_test[feat].apply(lambda x: val_mean_dict[x])

    if drop_original:
        for feat in feats:
            X_train = X_train.drop(feat, 1)
            X_test = X_test.drop(feat, 1)
            cat_feats.remove(feat)


    return X_train, X_test, y_train, y_test, cat_feats

def square(feats:list=[], drop_original=True):
    return functools.partial(_square, feats=feats, drop_original=drop_original)

@main_pipe('square')
def _square(X_train, X_test, y_train, y_test, cat_feats:list, *args, **kwargs):
    feats = kwargs['feats']
    target = kwargs['stonks_info']['target_var_name']
    drop_original = kwargs['drop_original']

    cat_feats = set(cat_feats)
    for feat in feats:
        assert feat not in cat_feats
        X_train['{}_square'.format(feat)] = X_train[feat].apply(np.square)
        X_test['{}_square'.format(feat)] = X_test[feat].apply(np.square)

    if drop_original:
        for feat in feats:
            X_train = X_train.drop(feat, 1)
            X_test = X_test.drop(feat, 1)


    return X_train, X_test, y_train, y_test, cat_feats

def sqrt(feats:list=[], drop_original=True):
    return functools.partial(_sqrt, feats=feats, drop_original=drop_original)

@main_pipe('sqrt')
def _sqrt(X_train, X_test, y_train, y_test, cat_feats:list, *args, **kwargs):
    feats = kwargs['feats']
    target = kwargs['stonks_info']['target_var_name']
    drop_original = kwargs['drop_original']

    cat_feats = set(cat_feats)
    for feat in feats:
        assert feat not in cat_feats
        X_train['{}_sqrt'.format(feat)] = X_train[feat].apply(np.sqrt)
        X_test['{}_sqrt'.format(feat)] = X_test[feat].apply(np.sqrt)

    if drop_original:
        for feat in feats:
            X_train = X_train.drop(feat, 1)
            X_test = X_test.drop(feat, 1)


    return X_train, X_test, y_train, y_test, cat_feats

def log(feats:list=[], drop_original=True):
    return functools.partial(_log, feats=feats, drop_original=drop_original)

@main_pipe('log')
def _log(X_train, X_test, y_train, y_test, cat_feats:list, *args, **kwargs):
    feats = kwargs['feats']
    target = kwargs['stonks_info']['target_var_name']
    drop_original = kwargs['drop_original']

    cat_feats = set(cat_feats)
    for feat in feats:
        assert feat not in cat_feats
        X_train['{}_log'.format(feat)] = X_train[feat].apply(np.log)
        X_test['{}_log'.format(feat)] = X_test[feat].apply(np.log)

    if drop_original:
        for feat in feats:
            X_train = X_train.drop(feat, 1)
            X_test = X_test.drop(feat, 1)


    return X_train, X_test, y_train, y_test, cat_feats

def float_to_str(feats:list=[], drop_original=True):
    return functools.partial(_float_to_str, feats=feats, drop_original=drop_original)

@main_pipe('float to str')
def _float_to_str(X_train, X_test, y_train, y_test, cat_feats:list, *args, **kwargs):
    feats = kwargs['feats']
    target = kwargs['stonks_info']['target_var_name']
    drop_original = kwargs['drop_original']

    cat_feats = set(cat_feats)
    for feat in feats:
        X_train['{}_str'.format(feat)] = X_train[feat].apply(lambda x: str(x))
        X_test['{}_str'.format(feat)] = X_test[feat].apply(lambda x: str(x))
        if feat in cat_feats:
            cat_feats.add('{}_str'.format(feat))

    if drop_original:
        for feat in feats:
            X_train = X_train.drop(feat, 1)
            X_test = X_test.drop(feat, 1)
            if feat in cat_feats:
                cat_feats.remove(feat)
        
    print(X_train.columns)


    return X_train, X_test, y_train, y_test, cat_feats

def to_int(feats:list=[], drop_original=True):
    return functools.partial(_to_int, feats=feats, drop_original=drop_original)

@main_pipe('to int')
def _to_int(X_train, X_test, y_train, y_test, cat_feats:list, *args, **kwargs):
    feats = kwargs['feats']
    target = kwargs['stonks_info']['target_var_name']
    idx = kwargs['stonks_info']['index_var_name']
    drop_original = kwargs['drop_original']

    cat_feats = set(cat_feats)

    if not feats:
        feats = set(X_train.columns) - set([target, idx])
    def convert_to_int(x):
        if pd.isna(x):
            return x
        try:
            return int(x)
        except:
            print('==={}=== error convert to int'.format(x))
            return np.NaN

    for feat in feats:
        X_train['{}_int'.format(feat)] = X_train[feat].apply(convert_to_int)
        X_test['{}_int'.format(feat)] = X_test[feat].apply(convert_to_int)
        if feat in cat_feats:
            cat_feats.add('{}_int'.format(feat))

    if drop_original:
        for feat in feats:
            X_train = X_train.drop(feat, 1)
            X_test = X_test.drop(feat, 1)
            if feat in cat_feats:
                cat_feats.remove(feat)

    return X_train, X_test, y_train, y_test, cat_feats


def na_to_cat(feats:list=[], drop_original=True):
    return functools.partial(_na_to_cat, feats=feats, drop_original=drop_original)

@main_pipe('missing value as category')
def _na_to_cat(X_train, X_test, y_train, y_test, cat_feats:list, *args, **kwargs):
    feats = kwargs['feats']
    target = kwargs['stonks_info']['target_var_name']
    drop_original = kwargs['drop_original']

    cat_feats = set(cat_feats)
    for feat in feats:
        X_train['{}_na_to_cat'.format(feat)] = X_train[feat].fillna('missing cat')
        X_test['{}_na_to_cat'.format(feat)] = X_test[feat].fillna('missing cat')
        if feat in cat_feats:
            cat_feats.add('{}_na_to_cat'.format(feat))

    if drop_original:
        for feat in feats:
            X_train = X_train.drop(feat, 1)
            X_test = X_test.drop(feat, 1)
            if feat in cat_feats:
                cat_feats.remove(feat)
        
    print(X_train.columns)


    return X_train, X_test, y_train, y_test, cat_feats

def arg_sort(feats:list=[], drop_original=False):
    return functools.partial(_arg_sort, feats=feats, drop_original=drop_original)

@main_pipe('arg sort')
def _arg_sort(X_train, X_test, y_train, y_test, cat_feats:list, *args, **kwargs):
    feats = kwargs['feats']
    target = kwargs['stonks_info']['target_var_name']
    idx = kwargs['stonks_info']['index_var_name']
    drop_original = kwargs['drop_original']

    cat_feats = set(cat_feats)

    if not feats:
        feats = set(X_train.columns) - set([target, idx])

    for feat in feats:
        X_train['{}_argsort'.format(feat)] = np.argsort(X_train[feat])
        X_test['{}_argsort'.format(feat)] = np.argsort(X_test[feat])
        if feat in cat_feats:
            cat_feats.add('{}_argsort'.format(feat))

    if drop_original:
        for feat in feats:
            X_train = X_train.drop(feat, 1)
            X_test = X_test.drop(feat, 1)
            if feat in cat_feats:
                cat_feats.remove(feat)

    return X_train, X_test, y_train, y_test, cat_feats


# def target_ratio(feats:list=[], drop_original=True):
#     return functools.partial(_target_ratio, feats=feats, drop_original=drop_original)

# @main_pipe('target ratio')
# def _target_ratio(X_train, X_test, y_train, y_test, cat_feats:list, *args, **kwargs):
#     feats = kwargs['feats']
#     target = kwargs['stonks_info']['target_var_name']
#     drop_original = kwargs['drop_original']

#     cat_feats = set(cat_feats)
#     for feat in feats:
#         assert feat not in cat_feats
#         X_train['{}_target_ratio'.format(feat)] = X_train[feat]/y_train
#         X_test['{}_target_ratio'.format(feat)] = X_test[feat]/X_test[target]

#     if drop_original:
#         for feat in feats:
#             X_train.drop(feat, 1)
#             X_test.drop(feat, 1)


#     return X_train, X_test, y_train, y_test, cat_feats

def cluster_feats(n_clusters=8, feats=[], drop_original=True):
    return functools.partial(_cluster_feats, n_clusters=n_clusters, feats=feats, drop_original=drop_original)

@main_pipe('cluster feats v5')
def _cluster_feats(X_train, X_test, y_train, y_test, cat_feats, *args, **kwargs):
    n_clusters = kwargs['n_clusters']
    feats = kwargs['feats']
    drop_original = kwargs['drop_original']

    kmeans = KMeans(random_state=42, n_clusters=n_clusters, n_jobs=-1)

    for feat in feats:
        assert feat not in cat_feats
    
    train_cluster = kmeans.fit_predict(X_train.loc[:,feats]).T
    test_cluster = kmeans.predict(X_test.loc[:,feats]).T

    print(train_cluster)
    print(np.unique(test_cluster))

    # print(train_cluster)

    # def f(x):
        # ind = np.argmax(x)
        # return ind

    # print(train_cluster.shape)
    # print(test_cluster.shape)

    feat_name = '{}_cluster'.format('_'.join(feats))
    X_train[feat_name] = train_cluster
    X_test[feat_name] = test_cluster

    # X_train['{}_cluster'.format('_'.join(feats))] = np.array(list(map(f,train_cluster))).T
    # X_test['{}_cluster'.format('_'.join(feats))] = np.array(list(map(f,test_cluster))).T

    if drop_original:
        for feat in feats:
            X_train = X_train.drop(feat, 1)
            X_test = X_test.drop(feat, 1)
    
    cat_feats = set(cat_feats)
    cat_feats.add(feat_name)



    return X_train, X_test, y_train, y_test, cat_feats