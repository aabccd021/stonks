from pipeline import main_pipe
from utils import get_feats
import functools
import pandas as pd

@main_pipe('del 0 var feats')
def remove_zero_variance_column(X_train, X_test, y_train, y_test, cat_feats:list, *args, **kwargs):
    no_variance_columns = [col_name for col_name, col_unique in X_train.nunique().to_dict().items() if col_unique == 1]
    X_train = X_train.drop(no_variance_columns, axis = 1)
    X_test = X_test.drop(no_variance_columns, axis = 1)
    print('dropped [{}] columns which has no variance : {}'.format(
        len(no_variance_columns),
        # ', '.join(str(no_variance_columns)),
        ', '.join(no_variance_columns),
    ))
    cat_feats = set([feat for feat in cat_feats if feat not in no_variance_columns])
    return X_train, X_test, y_train, y_test, cat_feats

def manual_drop(feats=None):
    return functools.partial(_manual_drop, feats=feats)

@main_pipe('manual drop')
def _manual_drop(X_train, X_test, y_train, y_test, cat_feats, *args, **kwargs):
    drop_feats = kwargs['feats']
    feats = get_feats(X_train, kwargs['stonks_info'])

    for drop_feat in drop_feats:
        if drop_feat not in feats:
            print(drop_feats, 'does not exist in train features')

    keep_feats = [feat for feat in feats if feat not in drop_feats]
    X_train = X_train[keep_feats]
    X_test = X_test[keep_feats]
    cat_feats = set([feat for feat in cat_feats if feat not in drop_feats])
    print('dropped features: {}'.format(', '.join(drop_feats)))
    return X_train, X_test, y_train, y_test, cat_feats

