from pipeline import main_pipe
import pandas as pd
import functools
import numpy as np
from catboost import CatBoostClassifier, CatBoostRegressor
from tqdm import tqdm

def fillna(method='median'):
    return functools.partial(_fillna, method=method)

@main_pipe('impute')
def _fillna(X_train:pd.DataFrame, X_test, y_train, y_test, cat_feats:list, *args, **kwargs):
    method = kwargs['method']
    if method == 'median':
        print('masuk')
        print(X_train.dtypes)
        X_train = X_train.fillna(X_train.median())
        for feat in cat_feats:
            X_train[feat] = X_train[feat].fillna(X_train[feat].mode()[0])
    if method == 'catboost':
        before = X_train.isnull().sum().sum()
        for feat_id, feat in enumerate(tqdm(X_train.columns)):
            train_X = X_train.dropna().drop(feat,1)
            train_y = X_train.dropna()[feat]
            if feat in cat_feats:
                model = CatBoostClassifier(verbose=0)
            else:
                model = CatBoostRegressor(verbose=0)
            model.fit(train_X, train_y)
            def fill_row_catboost(row):
                if pd.isnull(row[feat_id]):
                    if row.isnull().sum() == 1:
                        bahan = [x for x in row if not np.isnan(x)]
                        pred = model.predict(bahan)
                        return pred
                return row[feat_id]
                    
            X_train[feat] = X_train.apply(fill_row_catboost, axis=1)
            # for row_id, row in enumerate(X_train.itertuples()):
            #     if pd.isnull(X_train.iloc[row_id,feat_id]):
            #         if X_train.iloc[row_id].isnull().sum() == 1:
            #             bahan = [x for x in row if not np.isnan(x)]
            #             pred = model.predict(bahan)
            #             X_train.iloc[row_id, feat_id] = pred
        print('filled {} cells using catboost'.format(before - X_train.isnull().sum().sum()))
        X_train = X_train.fillna(X_train.median())

    X_test = X_test.fillna(X_test.median())
    for feat in cat_feats:
        X_test[feat] = X_test[feat].fillna(X_test[feat].mode()[0])
    print(X_train)
    print(X_train.isnull().sum())
    
    assert X_train.isnull().sum().sum() == 0
    assert X_test.isnull().sum().sum() == 0
    return X_train, X_test, y_train, y_test, cat_feats