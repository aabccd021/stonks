from stonks import Stonks
from pipeline import fit_predict, preproc_pipe, main_pipe
from feature_extraction import sequence_extract, count_occurence
from feature_selections import remove_zero_variance_column, manual_drop
from preprocessing import remove_not_in_test, set_index
from sklearn.metrics import f1_score
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC, LinearSVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.linear_model import LogisticRegression
from catboost import CatBoostClassifier
import time
import numpy as np
import pandas as pd
from tqdm import tqdm
import concurrent.futures
from joblib import Parallel, delayed
from normalization import box_cox
from sampling import smotenc
from imblearn.ensemble import BalancedBaggingClassifier, BalancedRandomForestClassifier
# from xgboost import XGBClassifier


@preproc_pipe('fix')
def column_name_fix(train, test, submit, cat_feats, *args, **kwargs):
    new_train = train.rename(columns={'hotel_id': 'is_cross_sell'})
    new_train['is_cross_sell'] = new_train['is_cross_sell'].apply(
        lambda x: 'no' if x == 'None' else 'yes')
    print('renamed train dataframe column hotel_id => is_cross_sell')
    return new_train, test, submit, cat_feats


def parallel_list_map(f, my_iter):
    with concurrent.futures.ThreadPoolExecutor() as executor:
        results = list(tqdm(executor.map(f, my_iter), total=len(my_iter)))
    return results

# @main_pipe('AcOcc')
# def count_account_id_occured(X_train, X_test, y_train, y_test, cat_feats:list, *args, **kwargs):
#     un_train = X_train['account_id'].unique()
#     un_test = X_test['account_id'].unique()

#     acc_dfs = []
#     for acc in tqdm(un_train):
#         acc_df = X_train[X_train['account_id']==acc][['log_transaction']]
#         acc_df['log_len'] = acc_df['log_transaction'].apply(lambda a: len([x for x in a.replace("'","").replace('[','').replace(']','').split(', ')]))
#         acc_df = acc_df.sort_values(by=['log_len'])
#         acc_df['acc_occured'] = range(1,acc_df.shape[0]+1)
#         acc_df = acc_df[['acc_occured']]
#         acc_dfs.append(acc_df)
#     acc_dfs = pd.concat(acc_dfs)
#     X_train = X_train.join(acc_dfs)

#     acc_dfs = []
#     for acc in tqdm(un_test):
#         acc_df = X_test[X_test['account_id']==acc][['log_transaction']]
#         acc_df['log_len'] = acc_df['log_transaction'].apply(lambda a: len([x for x in a.replace("'","").replace('[','').replace(']','').split(', ')]))
#         acc_df = acc_df.sort_values(by=['log_len'])
#         acc_df['acc_occured'] = range(1,acc_df.shape[0]+1)
#         acc_df = acc_df[['acc_occured']]
#         acc_dfs.append(acc_df)
#     acc_dfs = pd.concat(acc_dfs)
#     X_test = X_test.join(acc_dfs)

#     return X_train, X_test, y_train, y_test, cat_feats


@main_pipe('cat')
def set_cat_feats(X_train, X_test, y_train, y_test, cat_feats: set, *args, **kwargs):
    cat_feats.add('no_of_seats')
    train_seat = X_train['no_of_seats'].values
    test_seat = X_test['no_of_seats'].values
    intersect = np.intersect1d(train_seat, test_seat)
    print(intersect)

    X_train = X_train.join(y_train)
    X_test = X_test.join(y_test)
    X_train = X_train[X_train['no_of_seats'].isin(list(intersect))]
    X_test = X_test[X_test['no_of_seats'].isin(list(intersect))]
    y_train = X_train[['is_cross_sell']]
    y_test = X_test[['is_cross_sell']]
    # y_train = y_train.loc[X_train.index.values,:]
    # y_test = y_test.loc[X_train.index.values,:]

    return X_train, X_test, y_train, y_test, cat_feats


@main_pipe('lls')
def last_log_seq(X_train, X_test, y_train, y_test, cat_feats: list, *args, **kwargs):

    init_idx = X_train.index
    dict_train = {acc: None for acc in X_train['account_id'].unique()}
    feat_train = X_train[['account_id', 'log_transaction']]
    feat_train['log_transaction'] = feat_train['log_transaction'].apply(
        lambda a: [x for x in a.replace("'", "").replace('[', '').replace(']', '').split(', ')])
    feat_train['len_log_transaction'] = feat_train['log_transaction'].apply(
        lambda a: len(a))
    feat_train = feat_train.sort_values(by=['len_log_transaction'])
    last_log_seq = []
    for index, row in feat_train.iterrows():
        acc = row['account_id']
        seq = row['log_transaction']
        if dict_train[acc] == None:
            last_log_seq.append("'[{}]'".format(', '.join(seq)))
        else:
            last_len = len(dict_train[acc])
            last_log_seq.append("'[{}]'".format(', '.join(seq[last_len:])))
        dict_train[acc] = seq
    feat_train['last_log_seq'] = last_log_seq
    feat_train = feat_train[['last_log_seq']]
    X_train = X_train.join(feat_train)
    assert (X_train.index == init_idx).all()

    init_idx = X_test.index
    dict_test = {acc: None for acc in X_test['account_id'].unique()}
    feat_test = X_test[['account_id', 'log_transaction']]
    feat_test['log_transaction'] = feat_test['log_transaction'].apply(
        lambda a: [x for x in a.replace("'", "").replace('[', '').replace(']', '').split(', ')])
    feat_test['len_log_transaction'] = feat_test['log_transaction'].apply(
        lambda a: len(a))
    feat_test = feat_test.sort_values(by=['len_log_transaction'])
    last_log_seq = []
    for index, row in feat_test.iterrows():
        acc = row['account_id']
        seq = row['log_transaction']
        if dict_test[acc] == None:
            last_log_seq.append("'[{}]'".format(', '.join(seq)))
        else:
            last_len = len(dict_test[acc])
            last_log_seq.append("'[{}]'".format(', '.join(seq[last_len:])))
        dict_test[acc] = seq
    feat_test['last_log_seq'] = last_log_seq
    feat_test = feat_test[['last_log_seq']]
    X_test = X_test.join(feat_test)
    assert (X_test.index == init_idx).all()

    return X_train, X_test, y_train, y_test, cat_feats


@main_pipe('AcOcc')
def count_account_id_occured(X_train, X_test, y_train, y_test, cat_feats: list, *args, **kwargs):

    init_idx = X_train.index
    dict_train = {acc: 0 for acc in X_train['account_id'].unique()}
    feat_train = X_train[['account_id', 'log_transaction']]
    feat_train['log_transaction'] = feat_train['log_transaction'].apply(lambda a: len(
        [x for x in a.replace("'", "").replace('[', '').replace(']', '').split(', ')]))
    feat_train = feat_train.sort_values(by=['log_transaction'])
    acc_count = []
    for acc in tqdm(feat_train['account_id']):
        acc_count.append(dict_train[acc])
        dict_train[acc] += 1
    feat_train['acc_occured'] = acc_count
    feat_train = feat_train[['acc_occured']]
    X_train = X_train.join(feat_train)
    assert (X_train.index == init_idx).all()

    init_idx = X_test.index
    dict_test = {acc: 0 for acc in X_test['account_id'].unique()}
    feat_test = X_test[['account_id', 'log_transaction']]
    feat_test['log_transaction'] = feat_test['log_transaction'].apply(lambda a: len(
        [x for x in a.replace("'", "").replace('[', '').replace(']', '').split(', ')]))
    feat_test = feat_test.sort_values(by=['log_transaction'])
    acc_count = []
    for acc in tqdm(feat_test['account_id']):
        acc_count.append(dict_test[acc])
        dict_test[acc] += 1
    feat_test['acc_occured'] = acc_count
    feat_test = feat_test[['acc_occured']]
    X_test = X_test.join(feat_test)
    assert (X_test.index == init_idx).all()

    return X_train, X_test, y_train, y_test, cat_feats


@main_pipe('extPl')
def extract_places(X_train, X_test, y_train, y_test, cat_feats: list, *args, **kwargs):
    train_vc = X_train['visited_city'].values

    semarang = []
    jakarta = []
    medan = []
    bali = []
    jogjakarta = []
    manado = []
    aceh = []

    for city in train_vc:
        if 'Semarang' in city:
            semarang.append(1)
        else:
            semarang.append(0)

        if 'Jakarta' in city:
            jakarta.append(1)
        else:
            jakarta.append(0)

        if 'Medan' in city:
            medan.append(1)
        else:
            medan.append(0)

        if 'Bali' in city:
            bali.append(1)
        else:
            bali.append(0)

        if 'Jogjakarta' in city:
            jogjakarta.append(1)
        else:
            jogjakarta.append(0)

        if 'Manado' in city:
            manado.append(1)
        else:
            manado.append(0)

        if 'Aceh' in city:
            aceh.append(1)
        else:
            aceh.append(0)
    X_train['Semarang'] = semarang
    X_train['Jakarta'] = jakarta
    X_train['Medan'] = medan
    X_train['Bali'] = bali
    X_train['Jogjakarta'] = jogjakarta
    X_train['Manado'] = manado
    X_train['Aceh'] = aceh

    test_vc = X_test['visited_city'].values

    semarang = []
    jakarta = []
    medan = []
    bali = []
    jogjakarta = []
    manado = []
    aceh = []

    for city in test_vc:
        if 'Semarang' in city:
            semarang.append(1)
        else:
            semarang.append(0)

        if 'Jakarta' in city:
            jakarta.append(1)
        else:
            jakarta.append(0)

        if 'Medan' in city:
            medan.append(1)
        else:
            medan.append(0)

        if 'Bali' in city:
            bali.append(1)
        else:
            bali.append(0)

        if 'Jogjakarta' in city:
            jogjakarta.append(1)
        else:
            jogjakarta.append(0)

        if 'Manado' in city:
            manado.append(1)
        else:
            manado.append(0)

        if 'Aceh' in city:
            aceh.append(1)
        else:
            aceh.append(0)

    X_test['Semarang'] = semarang
    X_test['Jakarta'] = jakarta
    X_test['Medan'] = medan
    X_test['Bali'] = bali
    X_test['Jogjakarta'] = jogjakarta
    X_test['Manado'] = manado
    X_test['Aceh'] = aceh

    return X_train, X_test, y_train, y_test, cat_feats


@main_pipe('edan1v10')
def edan(X_train, X_test, y_train, y_test, cat_feats: list, *args, **kwargs):
    init_X_index = X_train.index

    # X_test['is_in_test'] = [1]*X_test.shape[0]
    # test_accs = X_test['account_id'].unique()
    # is_in_test = []
    # for acc in X_train['account_id'].values:
    #     if acc in test_accs:
    #         is_in_test.append(1)
    #     else:
    #         is_in_test.append(0)
    # X_train['is_in_test'] = is_in_test

    temp_X_train = X_train.join(y_train)
    temp_X_train['encoded_sell'] = temp_X_train['is_cross_sell'].apply(
        lambda x: 1 if x == 'yes' else 0)
    temp_X_train = temp_X_train[['account_id', 'encoded_sell']]
    aggregated = temp_X_train.groupby('account_id').agg(
        # ['min', 'max', 'median', 'mean', 'size', 'count', 'std', 'var', 'sem'])
        # ['min', 'max', 'median', 'mean', 'size', 'std', 'var', 'sem'])
        ['min', 'max', 'median', 'mean'])
    var = temp_X_train.groupby('account_id').var(ddof=0).rename(columns={'encoded_sell':'encoded_sell_var'})
    std = temp_X_train.groupby('account_id').std(ddof=0).rename(columns={'encoded_sell':'encoded_sell_std'})
    sem = temp_X_train.groupby('account_id').sem(ddof=0).rename(columns={'encoded_sell':'encoded_sell_sem'})
    print('X_train', X_train.shape)
    print('std', std.shape)
    print('var', var.shape)
    print('sem', sem.shape)
        # ['min', 'max', 'median', 'mean', 'std', 'var', 'sem'])
        # ['min', 'max', 'median', 'mean', 'var', 'sem'])
    X_train = X_train.join(var, on='account_id', how='left')
    X_train = X_train.join(std, on='account_id', how='left', lsuffix='_left', rsuffix='_right')
    X_train = X_train.join(sem, on='account_id', how='left', lsuffix='_kiri', rsuffix='_kanan')
    X_train = X_train.join(aggregated, on='account_id', how='left').fillna(0)
    X_test = X_test.join(var, on='account_id', how='left')
    X_test = X_test.join(std, on='account_id', how='left', lsuffix='_left', rsuffix='_right')
    X_test = X_test.join(sem, on='account_id', how='left', lsuffix='_kiri', rsuffix='_kanan')
    X_test = X_test.join(aggregated, on='account_id', how='left').fillna(0)

    assert (X_train.index == init_X_index).all()
    return X_train, X_test, y_train, y_test, cat_feats


@main_pipe('e3')
def edan2(X_train, X_test, y_train, y_test, cat_feats: list, *args, **kwargs):
    init_X_index = X_train.index

    # nums = ['price', 'member_duration_days', 'no_of_seats']
    # nums = ['price']
    # nums = ['log_transaction_first','log_transaction_max']
    for num in nums:
        temp_X_train = X_train[['account_id', num]]
        aggregated = temp_X_train.groupby('account_id').agg(
            ['min', 'max', 'median', 'mean', 'size', 'count', 'std', 'var', 'sem'])
            # ['var'])
        print(aggregated.columns)
        X_train = X_train.join(
            aggregated, on='account_id', how='left').fillna(0)
        X_test = X_test.join(aggregated, on='account_id', how='left').fillna(0)

    assert (X_train.index == init_X_index).all()
    return X_train, X_test, y_train, y_test, cat_feats


@main_pipe('lstflgh')
def last_flight_duration(X_train, X_test, y_train, y_test, cat_feats: list, *args, **kwargs):

    init_idx = X_train.index
    dict_train = {acc: None for acc in X_train['account_id'].unique()}
    feat_train = X_train[['account_id',
                          'log_transaction', 'member_duration_days']]
    feat_train['log_transaction'] = feat_train['log_transaction'].apply(
        lambda a: [x for x in a.replace("'", "").replace('[', '').replace(']', '').split(', ')])
    feat_train['len_log_transaction'] = feat_train['log_transaction'].apply(
        lambda a: len(a))
    feat_train = feat_train.sort_values(by=['len_log_transaction'])
    last_flight_duration = []
    for index, row in feat_train.iterrows():
        acc = row['account_id']
        dur = row['member_duration_days']
        if dict_train[acc] == None:
            last_flight_duration.append(0)
        else:
            last_flight_duration.append(dur-dict_train[acc])
        dict_train[acc] = dur
    feat_train['last_flight_duration'] = last_flight_duration
    feat_train = feat_train[['last_flight_duration']]
    X_train = X_train.join(feat_train)
    assert (X_train.index == init_idx).all()

    init_idx = X_test.index
    dict_test = {acc: None for acc in X_test['account_id'].unique()}
    feat_test = X_test[['account_id',
                        'log_transaction', 'member_duration_days']]
    feat_test['log_transaction'] = feat_test['log_transaction'].apply(
        lambda a: [x for x in a.replace("'", "").replace('[', '').replace(']', '').split(', ')])
    feat_test['len_log_transaction'] = feat_test['log_transaction'].apply(
        lambda a: len(a))
    feat_test = feat_test.sort_values(by=['len_log_transaction'])
    last_flight_duration = []
    for index, row in feat_test.iterrows():
        acc = row['account_id']
        dur = row['member_duration_days']
        if dict_test[acc] == None:
            last_flight_duration.append(dur)
        else:
            last_flight_duration.append(dur-dict_test[acc])
        dict_test[acc] = dur
    feat_test['last_flight_duration'] = last_flight_duration
    feat_test = feat_test[['last_flight_duration']]
    X_test = X_test.join(feat_test)
    assert (X_test.index == init_idx).all()

    return X_train, X_test, y_train, y_test, cat_feats


if __name__ == "__main__":
    start = time.time()

    models = [
        # (CatBoostClassifier, {'iterations':100, 'loss_function':'MultiClass'}),
        # (CatBoostClassifier, {'iterations':100, 'loss_function':'MultiClassOneVsAll'}),
        # (CatBoostClassifier, {'iterations':100, 'loss_function':'CrossEntropy'}),
        # (CatBoostClassifier, {'iterations':100,'scale_pos_weight':50}),
        # (CatBoostClassifier, {'iterations':100,'scale_pos_weight':40}),
        # (CatBoostClassifier, {'iterations':100,'scale_pos_weight':30}),
        # (CatBoostClassifier, {'iterations':100,'scale_pos_weight':25}),
        # (CatBoostClassifier, {'iterations':100,'scale_pos_weight':20}),
        # (CatBoostClassifier, {'iterations':100,'scale_pos_weight':15}),
        # (CatBoostClassifier, {'iterations':100,'scale_pos_weight':10}),
        # (CatBoostClassifier, {'iterations':100,'scale_pos_weight':5}),
        # (CatBoostClassifier, {'iterations':100}),
        # (CatBoostClassifier, {'iterations':1000,'scale_pos_weight':20}),
        # (CatBoostClassifier, {'iterations':1000}),
        # (KNeighborsClassifier(), {}),
        # (DecisionTreeClassifier, {}),
        # (DecisionTreeClassifier, {'class_weight':'balanced'}),
        # (RandomForestClassifier, {}),
        # (RandomForestClassifier, {'n_estimators':100, 'max_features':0.8,'n_jobs':-1}),
        # (RandomForestClassifier, {'n_estimators':100,'n_jobs':-1}),
        # (RandomForestClassifier, {'n_estimators':100, 'max_features':0.8}),
        # (RandomForestClassifier, {'n_estimators':150}),
        # (RandomForestClassifier, {'n_estimators':200}),
        # (RandomForestClassifier, {'n_estimators':100, 'max_features':0.8}),
        # (RandomForestClassifier, {'n_estimators':100, 'class_weight':'balanced'}),
        # (RandomForestClassifier, {'n_estimators':200, 'max_features':0.8,'n_jobs':-1}),
        # (RandomForestClassifier, {'n_estimators':100, 'max_features':0.5,'n_jobs':-1}),
        # (RandomForestClassifier, {'n_estimators':100, 'max_features':0.2,'n_jobs':-1}),
        # (RandomForestClassifier, {'n_estimators':100, 'bootstrap':True,'n_jobs':-1}),
        (RandomForestClassifier, {
         'n_estimators': 100, 'class_weight': 'balanced', 'n_jobs':-1}),
        # (RandomForestClassifier, {'n_estimators': 100,
                                #   'max_features': 'log2', 'n_jobs': -1}),
        # (RandomForestClassifier,
            # {'max_features': 0.05055623175038626, 'bootstrap': False, 'min_samples_leaf': 7, 'max_depth': 100, 'n_estimators': 477, 'min_samples_split': 8}),
        # (XGBClassifier, {}),
        # (BalancedBaggingClassifier, {'n_jobs':-1}),
        # (BalancedBaggingClassifier, {'n_estimators':100, 'n_jobs':-1}),
        # (BalancedRandomForestClassifier, {'n_jobs':-1}),
        # (BalancedRandomForestClassifier, {'n_estimators':100, 'n_jobs':-1}),
        # (RandomForestClassifier, {'n_estimators':150}),
        # (RandomForestClassifier, {'n_estimators':150, 'class_weight':'balanced'}),
        # (RandomForestClassifier, {'n_estimators':200}),
        # (RandomForestClassifier, {'n_estimators':200, 'class_weight':'balanced'}),
        # (RandomForestClassifier, {'n_estimators':100, 'class_weight':'balanced_subsample'}),
        # (MLPClassifier, {'alpha':1, 'max_iter':100}),
        # (GaussianNB(), {}),
        # (QuadraticDiscriminantAnalysis, {}),
        # (LogisticRegression, {'multi_class':'ovr'}),
        # (LogisticRegression, {}),
        # (LogisticRegression, {'class_weight':'balanced'}),
        # (LinearSVC, {})
        # (SVC,{'C':1,'gamma':1,'class_weight':None}),
        # (SVC,{'class_weight':'balanced'}),
        # (SVC,{}),
    ]

    seq_var = [
        # sequence_extract(feats=['log_transaction'], params=['mean']),
        # sequence_extract(feats=['log_transaction'], params=['median']),
        # sequence_extract(feats=['log_transaction'], params=['last']),
        # sequence_extract(feats=['log_transaction'], params=['first','len']),
        # sequence_extract(feats=['log_transaction'], params=['first','last']),
        # sequence_extract(feats=['log_transaction'], params=['first']),
        sequence_extract(feats=['log_transaction'], params=['first', 'max']),
        # sequence_extract(feats=['log_transaction'], params=['first','max','len']),
        # sequence_extract(feats=['log_transaction'], params=['first','max','last']),
        # sequence_extract(feats=['log_transaction'], params=['first','max','first']),
        # sequence_extract(feats=['log_transaction'], params=['first','max','min']),
        # sequence_extract(feats=['log_transaction'], params=['first','max','median']),
        # sequence_extract(feats=['log_transaction'], params=['first','max','q0.4']),
        # sequence_extract(feats=['log_transaction'], params=['first','max','q0.6']),
        # sequence_extract(feats=['log_transaction'], params=['first','max','q0.8']),
        # sequence_extract(feats=['log_transaction'], params=['q0.2']),
        # sequence_extract(feats=['log_transaction'], params=['q0.4']),
        # sequence_extract(feats=['log_transaction'], params=['q0.6']),
        # sequence_extract(feats=['log_transaction'], params=['q0.8']),
        # sequence_extract(feats=['log_transaction'], params=['max']),
        # sequence_extract(feats=['log_transaction'], params=['min']),
        # sequence_extract(feats=['log_transaction'], params=['len']),
        # sequence_extract(feats=['log_transaction'], params=[]),
    ]

    # removes = [
    #     'member_duration_days',
    #     'gender',
    #     'trip',
    #     'service_class',
    #     'price',
    #     'is_tx_promo',
    #     'no_of_seats',
    #     'airlines_name',
    #     'visited_city',
    # ]

    for model, params in models:
        print(model)
        preprocess = [
            set_index,
            column_name_fix,
            remove_not_in_test,
        ]

        main = [
            # set_cat_feats,
            # count_account_id_occured,
            # seq,
            # last_flight_duration,
            # count_occurence(feats=['account_id']),
            # manual_drop(feats=['account_id','log_transaction','no_of_seats',st_log_seq,
            # last_log_seq,
            # sequence_extract(feats=['log_transaction','last_log_seq'], params=['first','max']),
            sequence_extract(feats=['log_transaction'],
                             params=['first','max']),
            edan,
            # edan2,
            # count_occurence(feats=['account_id']),
            # manual_drop(feats=['account_id','log_transaction','no_of_seats','is_tx_promo','service_class','trip']),
            # manual_drop(feats=['account_id', 'log_transaction', 'no_of_seats',
            #    'is_tx_promo', 'service_class', 'trip', 'visited_city', 'gender']),
            # manual_drop(feats=['account_id','log_transaction','no_of_seats','is_tx_promo','service_class','trip','last_log_seq']),
            # manual_drop(feats=['account_id','log_transaction']),
            manual_drop(
                # feats=['account_id', 'log_transaction', 'visited_city', 'last_log_seq']),
                feats=['account_id', 'log_transaction', 'visited_city']),
            # manual_drop(feats=['account_id','log_transaction','is_tx_promo','service_class','trip']),
            remove_zero_variance_column,
            # box_cox(['member_duration_days']),
            # smotenc,
            # extract_places,
            # manual_drop(feats=['visited_city']),
            fit_predict(
                model=model,
                model_kwargs=params,
            )
        ]

        stonks = Stonks(
            target_var_name='is_cross_sell',
            index_var_name='order_id',
            train_file_name='flight.csv',
            cv=5,
            preprocess=preprocess,
            eval_func=f1_score,
            eval_kwargs={
                # 'pos_label': 'yes',
                'average': 'micro',
            },
        )

        stonks.eval(main)

    print("total time:", time.time()-start)
