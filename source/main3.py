from stonks import Stonks
from pipeline import fit_predict, preproc_pipe, main_pipe
from feature_extraction import sequence_extract, count_occurence
from feature_selections import remove_zero_variance_column, manual_drop
from impute import fillna
from preprocessing import remove_not_in_test, set_index, remove_duplicate_column
from sklearn.metrics import f1_score
from sklearn.ensemble import RandomForestRegressor
import sklearn.linear_model
import sklearn.neighbors
import sklearn.neural_network
import sklearn.svm
import sklearn.tree
import sklearn.ensemble
from catboost import CatBoostRegressor
import sklearn
import time
import numpy as np
import pandas as pd
from tqdm import tqdm
import concurrent.futures
from joblib import Parallel, delayed
from normalization import box_cox
from sampling import smotenc
from dimensionality_reduction import pca
from feature_extraction import target_encoding
from xgboost import XGBRegressor

def rmspe(y_true, y_pred):
    squea = np.square( (y_true - y_pred)/y_true)
    # print('squea',len(squea))
    return np.sqrt(np.mean(squea))


def parallel_list_map(f, my_iter):
    with concurrent.futures.ThreadPoolExecutor() as executor:
        results = list(tqdm(executor.map(f, my_iter), total=len(my_iter)))
    return results

if __name__ == "__main__":
    start = time.time()

    models = [
        # (RandomForestRegressor, {}),
        # (RandomForestRegressor, {'n_estimators': 100, 'n_jobs':-1}),
        # (sklearn.linear_model.LinearRegression,{}),
        # (sklearn.linear_model.Ridge,{}),
        # (CatBoostRegressor,{'verbose':0}),

        (CatBoostRegressor,{'thread_count':-1,'iterations':460, 'verbose':0, 'loss_function':'MAE'}),
        # (CatBoostRegressor,{'thread_count':-1,'iterations':460, 'verbose':0, 'loss_function':'MAE', 'depth':3, 'l2_leaf_reg':10}),
        # (CatBoostRegressor,{'thread_count':-1,'iterations':460, 'verbose':0, 'loss_function':'MAE', 'l2_leaf_reg':10, 'learning_rate':0.01 }),
        # (CatBoostRegressor,{'thread_count':-1, 'verbose':0, 'loss_function':'MAE', 'depth':3, 'learning_rate':0.01}),
        # (CatBoostRegressor,{'thread_count':-1, 'verbose':0, 'loss_function':'MAE', 'depth':4, 'l2_leaf_reg':10}),
        # (CatBoostRegressor,{'thread_count':-1, 'verbose':0, 'loss_function':'MAE', 'depth':4, 'learning_rate':0.01}),
        # (CatBoostRegressor,{'verbose':0, 'loss_function':'LogLinQuantile'}),
        # (CatBoostRegressor,{'verbose':0, 'loss_function':'Lq'}),
        # (CatBoostRegressor,{'verbose':0, 'loss_function':'Huber'}),
        # (CatBoostRegressor,{'verbose':0, 'loss_function':'Expectile'}),
        # (XGBRegressor, {}),

        # (sklearn.linear_model.Lasso,{}),
        # (sklearn.linear_model.BayesianRidge,{}),
        # (sklearn.neighbors.KNeighborsRegressor,{}),
        # (sklearn.neighbors.RadiusNeighborsRegressor,{}),
        # (sklearn.neural_network.MLPRegressor,{}),
        # (sklearn.svm.LinearSVR,{}),
        # (sklearn.svm.NuSVR,{}),
        # (sklearn.svm.SVR,{}),
        # (sklearn.tree.DecisionTreeRegressor,{}),
        # (sklearn.tree.ExtraTreeRegressor,{}),
    ]
    # models = [
        # (CatBoostRegressor,{'thread_count':-1,'iterations':i, 'verbose':0, 'loss_function':'MAE'}) for i in range(450, 480)
        # (CatBoostRegressor,{'thread_count':-1,'iterations':i, 'verbose':0, 'loss_function':'MAE'}) for i in range(460, 461)
    # ]

    risuto = []

    for model, params in models:
        for i in range (1, 14):
            print()
            print()
            print('start',model, params)
            preprocess = [
                set_index,
                # remove_not_in_test,
                remove_duplicate_column,
            ]

            main = [
                manual_drop(feats=['secret_10']),
                remove_zero_variance_column,
                fillna(method='catboost'),
                # fillna(method='median'),
                # target_encoding(
                #     # drop_original=False,
                #     feats=[
                #         'facility_1',
                #         'facility_2',
                #         'facility_3',
                #         'facility_4',
                #         'facility_5',
                #         'female',
                #         'male',
                #         'item_1',
                #         'item_2',
                #         'item_3',
                #         'item_4',
                #         'item_5',
                #         'secret_1',
                #         'secret_3',
                #         'secret_4',
                #         'secret_6',
                #         'secret_8',
                #         ], ),
                pca(n_components=i),
                # box_cox(feats=[
                    # 'distance_poi_A1',
                    # 'distance_poi_A2',
                    # 'distance_poi_A3',
                    # 'distance_poi_A4',
                    # 'distance_poi_A5',
                    # 'distance_poi_A6',
                    # 'distance_poi_B1',
                    # 'distance_poi_B2',
                    # 'distance_poi_B3',
                    # 'distance_poi_B4',
                    # 'room_size',
                    # ]),
                fit_predict(
                    model=model,
                    model_kwargs=params,
                    # use_one_hot=False,
                )
            ]

            stonks = Stonks(
                target_var_name='is_canceled',
                index_var_name='id',
                cv=5,
                preprocess=preprocess,
                eval_func=sklearn.metrics.accuracy_score,
                problem='classification',
                cat_feats={
                    'meal',
                    'country',
                    'market_segment',
                    'distribution_channel',
                    'd',
                }
            )

            score = stonks.eval(main)
            risuto.append((i, score))

    for el in risuto:
        print(el)
    print("total time:", time.time()-start)
