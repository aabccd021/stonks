from stonks import Stonks
from pipeline import fit_predict, preproc_pipe, main_pipe
from feature_extraction import sequence_extract, count_occurence, float_to_str, na_to_cat
from feature_selections import remove_zero_variance_column, manual_drop
from impute import fillna
from preprocessing import remove_not_in_test, set_index, remove_duplicate_column, sample_train
from sklearn.metrics import f1_score
from sklearn.ensemble import RandomForestRegressor
import sklearn.linear_model
import sklearn.neighbors
import sklearn.neural_network
import sklearn.svm
import sklearn.tree
import sklearn.ensemble
from catboost import CatBoostRegressor
import sklearn
import time
import numpy as np
import pandas as pd
from tqdm import tqdm
import concurrent.futures
from joblib import Parallel, delayed
from normalization import box_cox
from sampling import smotenc
from dimensionality_reduction import pca
from feature_extraction import target_encoding
from xgboost import XGBRegressor
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC, LinearSVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.linear_model import LogisticRegression
from catboost import CatBoostClassifier

def rmspe(y_true, y_pred):
    squea = np.square( (y_true - y_pred)/y_true)
    # print('squea',len(squea))
    return np.sqrt(np.mean(squea))


def parallel_list_map(f, my_iter):
    with concurrent.futures.ThreadPoolExecutor() as executor:
        results = list(tqdm(executor.map(f, my_iter), total=len(my_iter)))
    return results

if __name__ == "__main__":
    start = time.time()

    models = [
        # (RandomForestClassifier, {
        #  'n_estimators': 100, 'class_weight': 'balanced', 'n_jobs':-1}),
        (CatBoostClassifier, {'verbose':0, }),
        (RandomForestClassifier, {}),
        (LogisticRegression, {}),
        (DecisionTreeClassifier, {}),
        (KNeighborsClassifier, {}),
    ]

    risuto = []

    for model, params in models:
        print('start',model, params)
        preprocess = [
            set_index,
            sample_train,
            # remove_not_in_test,
            # remove_duplicate_column,
        ]

        main = [
            manual_drop(feats=[
                'reservation_status_date',
                'reservation_status',
                ]),
            remove_zero_variance_column,
            na_to_cat(feats=[
                'agent',
                'company',
            ]),
            fillna(method='median'),
            float_to_str(feats=[
                'agent_na_to_cat',
                'company_na_to_cat',
                # 'reservation_status_date'
                ]),
            # target_encoding(
            #     # drop_original=False,
            #     feats=[], ),
            # box_cox(feats=[]),
            fit_predict(
                model=model,
                model_kwargs=params,
                # use_one_hot=False,
            )
        ]

        stonks = Stonks(
            target_var_name='is_canceled',
            index_var_name='id',
            cv=5,
            preprocess=preprocess,
            eval_func=
            # [
                sklearn.metrics.accuracy_score,
                # sklearn.metrics.f1_score,
            # ],
            problem='classification',
            cat_feats={
                'hotel',
                'meal',
                'arrival_date_month',
                'country',
                'market_segment',
                'distribution_channel',
                'is_repeated_guest',
                'reserved_room_type',
                'assigned_room_type',
                'deposit_type',
                'agent',
                'company',
                'customer_type',
                'reservation_status',
            }
        )

        score = stonks.eval(main)
    print("total time:", time.time()-start)
