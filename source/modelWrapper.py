import sklearn
from utils import get_pipe_string, load_val_data, save_fold_result, \
    get_ori_submit_csv, save_submit, fold_result_exists, submit_exists, \
    load_val_cat_feats, get_int_mapping, map_cat_to_int, map_int_to_cat, \
        get_cat_feats_indices
import pandas as pd
from pipeline import main_pipe
import numpy as np
import catboost


class StonksWrapper:
    @staticmethod
    def get_model(ori_model, model_kwargs):
        if issubclass(ori_model, sklearn.base.BaseEstimator):
            return StonksSklearnWrapper(ori_model, model_kwargs)
        elif issubclass(ori_model, catboost.core.CatBoost):
            return StonksCatboostWrapper(ori_model, model_kwargs)
        else:
            import inspect
            print(inspect.getmro(ori_model))
            assert 1 == 2
            return StonksWrapper

    def __init__(self, ori_model, model_kwargs):
        self.model_kwargs = model_kwargs
        self.ori_model = ori_model(**model_kwargs)

    def fit_predict_eval(self, input_pipe, stonks_info, fit_kwargs={}):
        eval_func = stonks_info['eval_func']
        eval_kwargs = stonks_info['eval_kwargs']
        problem = stonks_info['problem']

        param_blacklist = ['n_jobs']
        model_params = ['{}:{}'.format(key, value) for key, value in self.model_kwargs.items() if key not in param_blacklist]
        model_params = sorted(model_params)
        model_params_string = ', '.join(model_params)
        result_pipe = input_pipe + \
            ["{} ({})".format(self.ori_model.__class__.__name__,
                           model_params_string)]
        result_name = get_pipe_string(result_pipe)

        if not fold_result_exists(result_name):
            # print('fold result not exist')
            data_name = get_pipe_string(input_pipe)
            X_train, X_test, y_train, y_test, cat_feats = load_val_data(
                data_name)

            # print('feats', X_train.columns)

            if problem == 'classification':

                mapping = get_int_mapping(y_train)

                print(X_train.columns[28])
                self.ori_model.fit(X_train, map_cat_to_int(
                    y_train, mapping).values.ravel(), **fit_kwargs)
                y_pred = self.ori_model.predict(X_test).flatten()

                y_pred = map_int_to_cat(y_pred, mapping)

            elif problem == 'regression':
                self.ori_model.fit(X_train, y_train.values.ravel())
                y_pred = self.ori_model.predict(X_test).flatten()

            assert len(y_pred) == y_test.shape[0]
            eval_result = eval_func(np.array(y_test.iloc[:,0]), np.array(y_pred), **eval_kwargs)
            # assert eval_result > 0

            save_fold_result(result_pipe, X_train, X_test,
                             y_train, y_test, y_pred, cat_feats, eval_result)
            print(eval_result)
        return result_pipe

    def fit_predict_submit(self, input_pipe, stonks_info, fit_kwargs={}):
        problem = stonks_info['problem']

        param_blacklist = ['n_jobs']
        model_params = ['{}:{}'.format(key, value) for key, value in self.model_kwargs.items() if key not in param_blacklist]
        model_params = sorted(model_params)
        model_params_string = ', '.join(model_params)
        output_pipe = input_pipe + \
            ["{} ({})".format(self.ori_model.__class__.__name__,
                           model_params_string)]
        submit_name = get_pipe_string(output_pipe)



        if not submit_exists(submit_name):

            data_name = get_pipe_string(input_pipe)
            X_train, X_test, y_train, y_test, cat_feats = load_val_data(
                data_name)

            print('feats', X_train.columns)

            if problem == 'classification':

                mapping = get_int_mapping(y_train)

                self.ori_model.fit(X_train, map_cat_to_int(
                    y_train, mapping).values.ravel(), **fit_kwargs)
                y_pred = self.ori_model.predict(X_test).flatten()
                y_pred = map_int_to_cat(y_pred, mapping)

            elif problem == 'regression':
                self.ori_model.fit(X_train, y_train.values.ravel())
                y_pred = self.ori_model.predict(X_test).flatten()

            # self.ori_model.fit(X_train, y_train.values.ravel(), **fit_kwargs)
            # y_pred = self.ori_model.predict(X_test)

            sample_submission = get_ori_submit_csv()

            assert X_test.shape[0] == sample_submission.shape[0]
            index_var = stonks_info['index_var_name']
            assert (X_test.index.values ==
                    sample_submission[index_var].values).all()
            assert np.array_equal(X_test.index.values,
                                  sample_submission[index_var].values)

            target = stonks_info['target_var_name']
            sample_submission[target] = y_pred
            submission = sample_submission

            assert type(submission) == pd.DataFrame
            save_submit(submit_name, X_train, X_test,
                        y_train, submission, cat_feats)

        return output_pipe


class StonksSklearnWrapper(StonksWrapper):

    def fit_predict_eval(self, input_pipe, stonks_info, use_one_hot, fit_kwargs={}):
        if use_one_hot:
            input_pipe = one_hot(pipes=input_pipe)
        return super().fit_predict_eval(input_pipe, stonks_info, fit_kwargs=fit_kwargs)

    def fit_predict_submit(self, input_pipe, stonks_info, use_one_hot,  fit_kwargs={}):
        if use_one_hot:
            input_pipe = one_hot(pipes=input_pipe)
        return super().fit_predict_submit(input_pipe, stonks_info, fit_kwargs=fit_kwargs)


class StonksCatboostWrapper(StonksWrapper):
    def fit_predict_eval(self, input_pipe, stonks_info, use_one_hot, fit_kwargs={}):
        fit_kwargs = {
            'cat_features': get_cat_feats_indices(input_pipe),
            'verbose': 0,
        }
        return super().fit_predict_eval(input_pipe, stonks_info, fit_kwargs=fit_kwargs)

    def fit_predict_submit(self, input_pipe, stonks_info, use_one_hot, fit_kwargs={}):
        fit_kwargs = {
            'cat_features': get_cat_feats_indices(input_pipe),
            'verbose': 0,
        }
        return super().fit_predict_submit(input_pipe, stonks_info, fit_kwargs=fit_kwargs)


@main_pipe('one-hot')
def one_hot(X_train, X_test, y_train, y_test, cat_feats, *args, **kwargs):
    for feat in cat_feats:
        X_train_dummy = pd.get_dummies(X_train[feat])
        cols = X_train_dummy.columns
        X_train_dummy.columns = ['{}_{}'.format(feat, col) for col in cols]
        X_train = pd.concat([X_train, X_train_dummy], axis=1)
        X_train = X_train.drop(feat, axis=1)

        X_test_dummy = pd.DataFrame(columns=cols)
        for col in cols:
            X_test_dummy[col] = X_test[feat].apply(lambda x: 1 if x == col else 0)
        X_test_dummy.columns = ['{}_{}'.format(feat, col) for col in cols]
        # print(X_test_dummy)
        X_test = pd.concat([X_test, X_test_dummy], axis=1)
        X_test = X_test.drop(feat, axis=1)

    if X_train.shape[1] != X_test.shape[1]:
        print('X_train', X_train.columns)
        print('X_test', X_train.columns)
    print(X_train.head())
    assert X_train.shape[1] == X_test.shape[1]
    assert X_train.isnull().sum().sum() == 0
    assert X_test.isnull().sum().sum() == 0
    return X_train, X_test, y_train, y_test, cat_feats
