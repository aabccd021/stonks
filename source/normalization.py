from pipeline import main_pipe
from scipy import stats
import functools

def box_cox(feats=[], zero_replace=0.001):
    return functools.partial(_box_cox, feats=feats, zero_replace=zero_replace)

@main_pipe('box-cox')
def _box_cox(X_train, X_test, y_train, y_test, cat_feats:list, *args, **kwargs):
    feats = kwargs['feats']
    zero_replace = kwargs['zero_replace']
    target = kwargs['stonks_info']['target_var_name']
    idx = kwargs['stonks_info']['index_var_name']

    if not feats:
        feats = set(X_train.columns) - set([target, idx])
    for feat in feats:
        a = stats.boxcox(X_train[feat].replace(0,zero_replace).values)[0]
        b = stats.boxcox(X_test[feat].replace(0,zero_replace).values)[0]
        X_train[feat] = a
        X_test[feat] = b
    return X_train, X_test, y_train, y_test, cat_feats