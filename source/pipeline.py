from utils import *
import functools


def preproc_pipe(name):
    def _preproc_pipe(func):
        def wrapper(*args, **kwargs):
            print('=== running preprocessing pipe [{}]'.format(name))
            prev_pipes = kwargs['pipes']

            if prev_pipes == []:
                prev_pipe_name = 'init'
            else:
                prev_pipe_name = get_pipe_string(prev_pipes)
            
            prev_pipes.append(name)
            save_pipe_name = get_pipe_string(prev_pipes)
            if is_data_exists(save_pipe_name):
                return prev_pipes
            
            print('=== pipe stringp: [{}]'.format(save_pipe_name))

            train, test, submit, cat_feats = load_data(prev_pipe_name)
            # print('cat_feats:', cat_feats)
            train, test, submit, cat_feats = func(train, test, submit, cat_feats, *args, **kwargs)

            assert type(train) == pd.DataFrame
            assert type(test) == pd.DataFrame
            assert type(submit) == pd.DataFrame
            assert type(cat_feats) == set

            save_data(save_pipe_name, train, test, submit, cat_feats)

            print()

            return prev_pipes
        return wrapper
    return _preproc_pipe

def main_pipe(name):
    def _main_pipe(func):
        def wrapper(*args, **kwargs):
            print()
            print('=== running main pipe [{}]'.format(name))
            # print('kwargs', kwargs)
            additional_kwargs = list(set(kwargs.keys()) - set(['pipes', 'stonks_info', 'mode']))
            additional_kwargs = sorted(additional_kwargs)
            additional_name = ""
            for arg in additional_kwargs:
                print(arg, kwargs[arg])
                additional_name += "({}:{})".format(arg, kwargs[arg]).replace("'","")

            prev_pipes = kwargs['pipes']

            if prev_pipes == []:
                prev_pipe_name = 'init'
            else:
                prev_pipe_name = get_pipe_string(prev_pipes)
            

            prev_pipes.append(name+additional_name)
            save_pipe_name = get_pipe_string(prev_pipes)
            print('=== pipe string: [{}]'.format(save_pipe_name))
            if is_val_data_exists(save_pipe_name):
                print('Cached')
                return prev_pipes

            # print('prev:',prev_pipe_name)
            X_train, X_test, y_train, y_test, cat_feats = load_val_data(prev_pipe_name)

            assert X_train.shape[1] == X_test.shape[1]
            assert X_train.shape[0] == y_train.shape[0]
            # assert X_test.shape[0] == y_test.shape[0]
            assert type(X_train) == pd.DataFrame
            assert type(y_train) == pd.DataFrame
            assert type(X_test) == pd.DataFrame
            assert type(y_test) == pd.DataFrame
            assert type(cat_feats) == set
            for feat in cat_feats:
                if feat not in X_train.columns:
                    print(feat)
                assert feat in X_train.columns


            X_train, X_test, y_train, y_test, cat_feats = func(X_train, X_test, y_train, y_test, cat_feats, *args, **kwargs)

            assert X_train.shape[1] == X_test.shape[1]
            assert X_train.shape[0] == y_train.shape[0]
            # assert X_test.shape[0] == y_test.shape[0]
            assert type(X_train) == pd.DataFrame
            assert type(y_train) == pd.DataFrame
            assert type(X_test) == pd.DataFrame
            assert type(y_test) == pd.DataFrame
            assert type(cat_feats) == set
            save_val_data(save_pipe_name, X_train, X_test, y_train, y_test, cat_feats)

            return prev_pipes
        return wrapper
    return _main_pipe

def fit_predict(model, model_kwargs, use_one_hot=True):
    return functools.partial(_fit_predict, model=model, model_kwargs=model_kwargs, use_one_hot=use_one_hot)

def _fit_predict(pipes, stonks_info, *args, **kwargs):
    from modelWrapper import StonksWrapper
    print('fit predict')
    model = kwargs['model']
    model_kwargs = kwargs['model_kwargs']
    use_one_hot = kwargs['use_one_hot']
    # eval_func = kwargs['stonks_info']['eval_func']

    model = StonksWrapper.get_model(model, model_kwargs)
    print(model.ori_model.__class__.__name__)
    mode = kwargs['mode']
    if mode == 'eval':
        pipes = model.fit_predict_eval(input_pipe=pipes, stonks_info=stonks_info, use_one_hot=use_one_hot)
        return pipes
    elif mode == 'submit':
        pipes = model.fit_predict_submit(input_pipe=pipes, stonks_info=stonks_info, use_one_hot=use_one_hot)
        return pipes

def tune(pipes, stonks_info, *args, **kwargs):
    model = kwargs['objective']
    study = optuna.create_study()
    study.optimize(objective, n_trials=100)

    print(study.best_params)
    print(study.best_value)
    print(study.best_trial)


    # X_train, X_test, y_train, y_test, cat_feats = load_val_data(get_pipe_string(pipes))
    # model.fit(X_train, y_train)
    # pred = model.predict(X_test)
    # eval_score = eval_func(y_test, pred)
    # print (model)
    # print (eval_score)
