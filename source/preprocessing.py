from pipeline import preproc_pipe
import numpy as np
import functools
from utils import *


@preproc_pipe('del cat feats not in test')
def remove_not_in_test(train, test, submit, cat_feats, *args, **kwargs):

    stonks_info = kwargs['stonks_info']
    index_name = stonks_info['index_var_name']
    target_name = stonks_info['target_var_name']

    rows_before = train.shape[0]

    train_nunique = train.nunique()

    for feat in cat_feats:
        uni_test = set(test[feat].unique())
        train = train[train[feat].isin(uni_test)]
    
    rows_after = train.shape[0]

    print('dropped [{}] rows'.format(rows_before - rows_after))
    
    return train, test, submit, cat_feats

@preproc_pipe('set index')
def set_index(train:pd.DataFrame, test:pd.DataFrame, submit:pd.DataFrame, cat_feats:list, *args, **kwargs):

    stonks_info = kwargs['stonks_info']
    index_name = stonks_info['index_var_name']
    train = train.set_index(index_name)
    test = test.set_index(index_name)
    submit = submit.set_index(index_name)

    return train, test, submit, cat_feats

@preproc_pipe('sample train')
def sample_train(train:pd.DataFrame, test:pd.DataFrame, submit:pd.DataFrame, cat_feats:list, *args, **kwargs):

    # frac = kwargs['frac']
    train = train.sample(frac=0.1, random_state=42)

    return train, test, submit, cat_feats
 
@preproc_pipe('remove duplicate')
def remove_duplicate_column(train:pd.DataFrame, test:pd.DataFrame, submit:pd.DataFrame, cat_feats:list, *args, **kwargs):

    stonks_info = kwargs['stonks_info']
    target_name = stonks_info['target_var_name']

    before = set(train.columns)

    not_duplicated = ~train.T.duplicated(keep='first')
    train = train.loc[:,not_duplicated]

    test_columns = list(train.columns)
    test_columns.remove(target_name)
    test = test.loc[:,test_columns]

    cat_feats = set([feat for feat in cat_feats if feat in train.columns])

    dropped = before - set(train.columns)
    print('removed {} duplicates: {}'.format(len(dropped), dropped))

    return train, test, submit, cat_feats
 


