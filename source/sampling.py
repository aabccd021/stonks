from imblearn.over_sampling import SMOTENC
from pipeline import main_pipe
from utils import get_cat_feats_indices2
import pandas as pd

# SMOTE to balance dataset
@main_pipe('SMOTE')
def smotenc(X_train, X_test, y_train, y_test, cat_feats: list, *args, **kwargs):
    target = kwargs['stonks_info']['target_var_name']

    cat_feat_indices = get_cat_feats_indices2(
        X_train.columns.values, cat_feats)
    sm = SMOTENC(categorical_features=cat_feat_indices)

    print('X_train', X_train.shape)
    print('y_train', y_train.shape)
    print(X_train.head(2))
    print(y_train.head(2))

    X_train, y_train_sample = sm.fit_sample(
        X_train, y_train[target])
    print('len', len(y_train_sample))

    y_train = pd.DataFrame(columns=[target])
    y_train[target] = y_train_sample

    print('X_train', X_train.shape)
    print('y_train', y_train.shape)
    print(X_train.head(2))
    print(y_train.head(2))

    return X_train, X_test, y_train, y_test, cat_feats
