import os
import json
import joblib
import pandas as pd
import numpy as np
import hashlib
from sklearn.model_selection import StratifiedKFold, KFold

config_json_path = os.path.join('.', 'config.json')


def load_data(name, train_only=False):
    data_dir = get_data_dir()
    save_dir = os.path.join(data_dir, name)
    train = joblib.load(os.path.join(save_dir, 'train.pkl'))
    test = joblib.load(os.path.join(save_dir, 'test.pkl'))
    submit = joblib.load(os.path.join(save_dir, 'submit.pkl'))
    cat_feats = joblib.load(os.path.join(save_dir, 'cat_feats.pkl'))
    return train, test, submit, cat_feats

# def save_cv_data(pipes:list, cv:int, stonks_info:dict):
#     data_dir = get_data_dir()
#     name = get_pipe_string(pipes)
#     load_dir = os.path.join(data_dir, name)

#     filename = '{}cv(1).pkl'.format(cv)
#     try:
#         ret = []
#         for fold in range(cv):
#             ret.append(joblib.load(os.path.join(save_dir,'{}cv({}).pkl'.format(cv, fold+1))))
#         return ret
#     except:
#     print('creating {} {}'.format(name, cv))
#     train, test, submit, cat_features = load_data(name)
#     cv_data = create_cv_data(df=train, cv=cv, stonks_info=stonks_info)
#     for fold, data in enumerate(cv_data):
#         joblib.dump(data, os.path.join(save_dir, '{}cv({}).pkl'.format(cv, fold+1)))
#     return pipe


# def create_cv_data(df:pd.DataFrame, cv:int, stonks_info:dict):
#     feats = get_feats(df, stonks_info)
#     target = stonks_info['target_var_name']
#     return ret
# def save_cv_result(name, scores_mean):
#     data_dir = get_data_dir()
#     save_path = os.path.join(data_dir, '{}.pkl'.format(new_pipe_name))
#     joblib.dump(scores_mean, )
# def save_fold_result(name, X_train, X_test, y_train, y_test, y_pred, cat_feats, result):
#     data_dir = get_data_dir()
#     save_dir = os.path.join(data_dir, name)
#     if not os.path.exists(save_dir):
#         os.makedirs(save_dir)
#     save_name = '{}.pkl'.format(name)
#     joblib.dump((X_train, X_test, y_train, y_test, y_pred,
#                  cat_feats), os.path.join(save_dir, save_name))
#     joblib.dump(result, os.path.join(save_dir, 'result.pkl'))

def get_ori_submit_csv():
    data_dir = get_data_dir()
    return joblib.load(os.path.join(data_dir, 'init', 'submit.pkl'))

def save_submit(name, X_train, X_test, y_train, submission:pd.DataFrame, cat_feats):
    data_dir = get_data_dir()
    save_dir = os.path.join(data_dir, name)
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    save_name = '{}.pkl'.format(name)
    joblib.dump((X_train, X_test, y_train, submission, 
                 cat_feats), os.path.join(save_dir, save_name))
    submission.to_csv(os.path.join(save_dir, 'submit.csv'), index=False)

def submit_exists(name):
    data_dir = get_data_dir()
    save_dir = os.path.join(data_dir, name)
    save_name = '{}.pkl'.format(name)
    return os.path.exists(os.path.join(save_dir, save_name)) and os.path.exists(os.path.join(save_dir, 'submit.csv'))

def save_cv_result(pipe, scores_mean, cv):
    name = get_pipe_string(pipe)
    pretty = pipe_pretty_print(pipe)
    data_dir = get_data_dir()
    save_dir = os.path.join(data_dir, name)
    joblib.dump(scores_mean, os.path.join(save_dir, 'cv{}_result.pkl'.format(cv)))
    with open(os.path.join(save_dir, 'pipe_desc.txt'), 'w') as f:
        f.write(pretty)


def get_submit_pipeline(pipes, stonks_info):
    data_dir = get_data_dir()
    old_name = get_pipe_string(pipes)

    new_pipes = pipes + ['submit']
    new_pipe_name = get_pipe_string(new_pipes)
    save_path = os.path.join(data_dir, '{}.pkl'.format(new_pipe_name))

    if os.path.exists(save_path):
        print('cached')

    else:
        train, test, submit, cat_feats = load_data(old_name)

        feats = get_feats(train, stonks_info)
        target = stonks_info['target_var_name']

        X_train = train[feats]
        y_train = train[[target]]
        X_test = test[feats]
        y_test = train[[target]]

        assert X_train.shape[1] == X_test.shape[1]
        assert X_train.shape[0] == y_train.shape[0]
        # assert X_test.shape[0] == y_test.shape[0]
        assert type(X_train) == pd.DataFrame
        assert type(y_train) == pd.DataFrame
        assert type(X_test) == pd.DataFrame
        assert type(y_test) == pd.DataFrame
 
        joblib.dump((X_train, X_test, y_train, y_test, cat_feats), save_path)
    
    return new_pipes



def get_cv_pipelines(pipes, cv, stonks_info, seed=42):
    print('=== creating {} fold'.format(cv))
    data_dir = get_data_dir()
    name = get_pipe_string(pipes)
    train, test, submit, cat_feats = load_data(name)

    feats = get_feats(train, stonks_info)
    target = stonks_info['target_var_name']
    problem = stonks_info['problem']
    print(train)
    X = train[feats]
    y = train[target]

    if problem == 'classification':
        skf = StratifiedKFold(n_splits=cv, random_state=seed, shuffle=True)
    elif problem == 'regression':
        skf = KFold(n_splits=cv, random_state=seed, shuffle=True)
    ret = []
    for fold, (train_index, test_index) in enumerate(skf.split(X, y)):
        print(fold+1, end=', ')
        new_pipe = pipes + ['cv:{} seed:{} fold_id:{} '.format(cv, seed, fold+1)]
        new_pipe_name = get_pipe_string(new_pipe)
        save_path = os.path.join(data_dir, '{}.pkl'.format(new_pipe_name))
        ret.append(new_pipe)
        if os.path.exists(save_path):
            print('cached')
            continue

        ii = np.array(train_index)
        X_train, X_test = X.iloc[train_index], X.iloc[test_index]
        y_train, y_test = pd.DataFrame(
            y.iloc[train_index]), pd.DataFrame(y.iloc[test_index])

        assert type(X_train) == pd.DataFrame
        assert type(X_test) == pd.DataFrame
        assert type(y_train) == pd.DataFrame
        assert type(y_test) == pd.DataFrame

        joblib.dump((X_train, X_test, y_train, y_test, cat_feats), save_path)

    print()

    return ret


def load_val_data(name):
    data_dir = get_data_dir()
    (X_train, X_test, y_train, y_test, cat_feats) = joblib.load(
        os.path.join(data_dir, '{}.pkl'.format(name)))
    return X_train, X_test, y_train, y_test, cat_feats

def load_val_cat_feats(pipe):
    name = get_pipe_string(pipe)
    data_dir = get_data_dir()
    (X_train, X_test, y_train, y_test, cat_feats) = joblib.load(
        os.path.join(data_dir, '{}.pkl'.format(name)))
    return cat_feats



def save_val_data(name, X_train, X_test, y_train, y_test, cat_feats):
    data_dir = get_data_dir()
    save_path = os.path.join(data_dir, '{}.pkl'.format(name))
    joblib.dump((X_train, X_test, y_train, y_test, cat_feats), save_path)


def load_fold_result(name, mode='.'):
    data_dir = get_data_dir(mode)
    save_dir = os.path.join(data_dir, name)
    return joblib.load(os.path.join(save_dir, 'result.pkl'))

def fold_result_exists(name):
    data_dir = get_data_dir()
    save_dir = os.path.join(data_dir, name)
    return os.path.exists(os.path.join(save_dir, 'result.pkl'))


def save_fold_result(pipe, X_train, X_test, y_train, y_test, y_pred, cat_feats, result):
    name = get_pipe_string(pipe)
    pretty_print_pipe =pipe_pretty_print(pipe)
    data_dir = get_data_dir()
    save_dir = os.path.join(data_dir, name)
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    save_name = '{}.pkl'.format(name)
    joblib.dump((X_train, X_test, y_train, y_test, y_pred,
                 cat_feats), os.path.join(save_dir, 'pipe.pkl'))
    joblib.dump(result, os.path.join(save_dir, 'result.pkl'))
    with open(os.path.join(save_dir, 'pipe_desc.txt'), 'w') as f:
        f.write(pretty_print_pipe)


def save_data(name, train, test, submit, cat_feats):
    data_dir = get_data_dir()
    save_dir = os.path.join(data_dir, name)
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    joblib.dump(train, os.path.join(save_dir, 'train.pkl'))
    joblib.dump(test, os.path.join(save_dir, 'test.pkl'))
    joblib.dump(submit, os.path.join(save_dir, 'submit.pkl'))
    joblib.dump(cat_feats, os.path.join(save_dir, 'cat_feats.pkl'))


def is_val_data_exists(name):
    data_dir = get_data_dir()
    check_data_dir = os.path.join(data_dir, "{}.pkl".format(name))
    if os.path.exists(check_data_dir):
        return True
    return False


def is_data_exists(name):
    data_dir = get_data_dir()
    check_data_dir = os.path.join(data_dir, name)
    if os.path.exists(check_data_dir):
        print('data [{}] already exists'.format(name))
        return True
    print('data [{}] does not exists'.format(name))
    return False


def get_data_dir(mode='.'):
    return os.path.join(mode, 'data', 'generated')
    # return os.path.join(mode, 'data', 'num_bench')


def get_raw_dir():
    return os.path.join('.', 'data')


def get_config():
    with open(config_json_path) as f:
        config = json.load(f)
    return config


def add_config(name, object):

    with open(config_json_path) as f:
        config = json.load(f)

    config[name] = object

    with open(config_json_path, 'w') as outfile:
        json.dump(config, outfile, indent=4)


def get_feats(df: pd.DataFrame, stonks_info: dict):
    index = stonks_info['index_var_name']
    target = stonks_info['target_var_name']
    cols = df.columns.to_list()
    feats = [col for col in cols if col not in [index, target]]
    return feats


def get_pipe_string(pipes):
    # def compress(name):
        # return name.replace('_','').replace(', ',' ')
    # pipes = [compress('{}'.format(pipe)) for pipe in pipes]
    res = '\n'.join(pipes)
    res = hashlib.sha1(res.encode('utf-8')).hexdigest()
    return res

def get_int_mapping(df:pd.DataFrame):
    uniques = np.unique(df.iloc[:,0].values)
    mapping = dict()
    for i, unique in enumerate(uniques):
        mapping[unique] = i
    return mapping

def map_cat_to_int(old_df:pd.DataFrame, mapping):
    df = old_df.copy(deep=True)
    col_name = df.columns[0]
    df[col_name] = df[col_name].apply(lambda x: mapping[x])
    return df

def map_int_to_cat(pred:np.array, mapping):
    reverse_map = dict()
    for cat, i in mapping.items():
        reverse_map[i] = cat
    pred = np.array(list(map(lambda x: reverse_map[x],pred)))
    return pred

def get_cat_feats_indices(pipe):
    name = get_pipe_string(pipe)
    X_train, X_test, y_train, y_test, cat_feats = load_val_data(name)
    feats = X_train.columns
    cat_feats_indices = []

    for ind, feat in enumerate(feats):
        if feat in cat_feats:
            cat_feats_indices.append(ind)
    return cat_feats_indices

def get_cat_feats_indices2(feats, cat_feats):
    cat_feats_indices = []
    for ind, feat in enumerate(feats):
        if feat in cat_feats:
            cat_feats_indices.append(ind)
    return cat_feats_indices

def pipe_pretty_print(pipes):
    res = '\n'.join(pipes)
    return res